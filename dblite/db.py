# -*- coding: utf-8 -*-
from .entity import Entity
from .relation import Relation
from .environment import Environment

from collections import OrderedDict
import json, os, shutil

class DBLite(Entity, Relation, Environment):
    '''
        Banco de dados simples usando Btree para índices, arquivo 
        serial para dados e trie e patricia para dados.
        IMPORTANTE: essa implementação sempre usa id como chave primária,
            logo não é necessário explicitala como um campo, ele será
            criado por default
    '''
    def __init__(self, path):
        '''
            Inicia banco de dados, para isso é necessário enviar o caminho
            da pasta onde o banco será iniciado.
        '''
        if os.path.isdir(path) == False:
            raise TypeError # erro ao procurar pelo diretório onde deveria ser criado banco de dados
        if not path[-1] == os.sep:
            path = path + os.sep
        if path.startswith('/'):
            if path.startswith('.'):
                path = path[1:]
            self.base_dir = path
        else:
            if path.startswith('.'):
                path = path[1:]
            self.base_dir = os.getcwd() + path
        self.db_dir = self.base_dir + '.db/'
        try:
            os.makedirs(self.db_dir, exist_ok=True)
        except OSError:
            raise TypeError # erro ao tentar criar banco de dados no disco
        Entity.__init__(self)
        Relation.__init__(self)

    def __repr__(self):
        print_string = 'DBLite save in ({})\nEntities:'.format(self.base_dir)
        for i in range(len(self.entities['name'])):
            print_string += '\n\t* {} = '.format(self.entities['name'][i])
            for field in self.entities['fields'][i].keys():
                print_string += '{}({}), '.format(field, \
                        self.entities['fields'][i][field])
        print_string += '\nRelations: '
        for rel_name, rel_type in \
                zip(self.relations['name'], self.relations['type']):
            print_string += '\n\t* {} ({}) --> ({}) {} '.format(rel_name[0],rel_type[0], 
                rel_type[1], rel_name[1])
        return print_string

    def delete(self):
        try:
            shutil.rmtree(self.db_dir)
            self.__init__(self.base_dir)
        except:
            raise TypeError # erro ao tentar excluir banco de dados     
        del self

    def configure(self, option = 'create'):
        '''
            Exporta a configuração do banco de dados como um json.
            Existem duas opções:
                create      = quando se está configurando pela primeira vez
                substitute  = quando deseja substitur a configuração atual
        '''
        if option != 'create' and option != 'substitute':
            raise TypeError # erro ao configurar banco de dados, necessário enviar uma opção válida
        if option == 'create':
            if(os.path.isfile(self.db_dir+'config.json')) == True:
                raise TypeError # erro ao configurar banco de dados, arquivo de configuração já está salvo
        with open(self.db_dir + 'config.json', 'w') as f:
            d = {'entities': self.entities, 'relations': self.relations}
            json.dump(d, f)
        f.close()
        self._setEnv()

    def connect(self):
        '''
            Cria uma conexão com o banco de dados no dicionário.
        '''
        if(os.path.isfile(self.db_dir+'config.json')) == False:
            raise TypeError # erro ao tentar connectar com o banco de dados, arquivo de configuração não está salvo
        with open(self.db_dir + 'config.json', 'r') as f:
            data = json.load(f, object_pairs_hook=OrderedDict)
            self.entities = data['entities']
            self.relations = data['relations']
    
         
