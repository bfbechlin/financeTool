#-*- coding utf-8 -*-
from .structtype import StructType
from struct import pack, unpack, calcsize
from collections import OrderedDict
from os import path

def chunks(data, n):
    '''
        'Yielda' n pedaços de um binário.
        Garanta que n é multiplo do tamanho do dado. Caso contrário o último
        pedaço será menor que os demais.
    '''
    l = int(len(data)/n)
    for i in range(0, len(data), l):
        yield data[i:i+l]

HEADER_LEN = 8

NODE_STRUCT = 'QQ?'
NODE_STRUCT_LEN = calcsize(NODE_STRUCT)
class BTree():
    '''
        Implementação de uma árvore B em disco. Argumentos:
        * File: nome do arquivo em será salvo a árvore. (Default: new.btree)
        * Fields: um dicionário de campos e seus respectivos tipos, que será
        a estrutura interna do árvore. (Default: {'id':'uint4'})
        * Primary_key: a chave primária da árvore, ou seja, o campo cujo será
        realizado o ordenamento. (Default: 'id')
        Lembrando que de preferencia a chaves primárias de números inteiros, mas
        nada impede que use outros tipos de chaves desde que sejam comparáveis
        através dos operadores disponível no builtins do python (<,>,=).
    '''
    def __init__(self, *args, **kwargs):
        if len(args) == 0:
            self.file = kwargs.get('file', 'new.btree')
            fields = kwargs.get('fields', {'id':'uint4'})
            self.primary_key = kwargs.get('primary', 'id')
        else:
            self.file = args[0]
            fields = args[1]
            self.primary_key = args[2]

        if not fields.get(self.primary_key):
            raise InvalidPrimaryKey('Chave primária não encontrada.')

        # Adicionando novo campo para filho
        fields['child_ptr'] = 'uint8'
        self.struct = StructType(fields, minimize = True)
        self.t = int((4096-NODE_STRUCT_LEN)/(2*len(self.struct)))
        # self.t = 2

        self.init_node = {
                'key':self.struct.index(self.primary_key),
                'child':self.struct.index('child_ptr'),
                'struct':self.struct,
                'file':self.file,
                't':self.t}
        self.key = self.init_node['key']
        self.child = self.init_node['child']
        self.block_s = len(self.struct)*2*self.t + NODE_STRUCT_LEN
        self.root = self.loadRoot()

    def __repr__(self):
        pt_str = 'BTREE at file: {}\nItens:\n'.format(self.file)
        for key, type in zip(self.struct.dic.keys(), self.struct.dic.values()):
            if not key == 'child_ptr':
                pt_str += '* {}: {}  {}\n'.format(key, type,\
                        'Primary Key' if key == self.primary_key else ' ')
        pt_str += 'Root: '
        for key in self.root.data[self.key]:
            if key != 0:
                pt_str += ' {},'.format(key)
        pt_str = pt_str[:-1]
        return pt_str

    def loadRoot(self):
        '''
            Carrega o nó raiz da árvore do disco para a memória. Caso o arquivo
            esteja vazio então cria um nó raiz.
        '''
        block_id = HEADER_LEN
        if path.isfile(self.file):
            with open(self.file, 'r+b') as f:
                data = f.read(HEADER_LEN)
                if data != b'':
                    f.close()
                    root =  self._blockCharge(unpack('Q', data)[0])
                else:
                    root = _BNode(HEADER_LEN, 0, True, self.init_node)
                    root.becameRoot()
                    root.store()
                    f.close()
        else:
            with open(self.file, 'wb') as f:
                root = _BNode(HEADER_LEN, 0, True, self.init_node)
                root.becameRoot()
                root.store()
                f.close()
        return root

    def searchRange(self, key_ini, key_fin):
        '''
            Procura por um intervalo de chaves na árvore.
        '''
        found = []
        def recursive(ptr):
            if ptr == 0:
                return
            else:
                node = self._blockCharge(ptr)
                lastkey = 0
                for i, key in enumerate(node.data[self.key]):
                    if key == 0:
                        break
                    elif key >= key_ini and key <= key_fin:
                        recursive(node.data[self.child][i])
                        a = self.struct.decodeItems([x[i] for x in node.data])
                        del a['child_ptr']
                        found.append(a)
                    elif lastkey <= key_fin:
                        recursive(node.data[self.child][i])
                    lastkey = key
                recursive(node.data[self.child][i+1])
        recursive(self.root.ptr)
        return found

    def listAll(self, inverse = False):
        found = []
        def recursive(ptr):
            if ptr == 0:
                return
            else:
                node = self._blockCharge(ptr)
                lastkey = 0
                for i, key in enumerate(node.data[self.key]):
                    if key == 0:
                        break
                    recursive(node.data[self.child][i])
                    a = self.struct.decodeItems([x[i] for x in node.data])
                    del a['child_ptr']
                    found.append(a)
                recursive(node.data[self.child][i+1])

        def recursiveInverse(ptr):
            if ptr == 0:
                return
            else:
                node = self._blockCharge(ptr)
                lastkey = 0
                for i, key in enumerate(node.data[self.key]):
                    if key == 0:
                        break
                    recursive(node.data[self.child][i])
                    a = self.struct.decodeItems([x[i] for x in node.data])
                    del a['child_ptr']
                    found.insert(0, a)
                recursive(node.data[self.child][i+1])
        if inverse == True:
            recursiveInverse(self.root.ptr)
        else:
            recursive(self.root.ptr)
        return found

    def search(self, key):
        if key == 0:
            raise InvalidKey('Chave 0 não é uma chave válida')
        i, node = self._searchKey(key)
        if i == self.t*2:
            return None
        elif key == node.data[self.key][i]:
            a = self.struct.decodeItems([x[i] for x in node.data])
            del a['child_ptr']
            return a
        else:
            return None

    def insert(self, data, exist = False):
        '''
            Insere um com os campos data na árvore, retornando true caso a insersão
            tenha sido realizada com sucesso.
            Caso a chave primária já exista na árvore, então retorna False a não ser
            que o parâmetro exist seja True, onde então retorna o item com essa
            chave que está na lista.
        '''
        data['child_ptr'] = 0
        data = self.struct.encodeItems(data)
        key = data[self.key]
        i, node = self._searchKey(key)

        if node.getKey(key)[1] == True:
            if exist == False:
                return False
            else:
                a = self.struct.decodeItems([x[i] for x in node.data])
                del a['child_ptr']
                return a

        while True:
            # Testando se nodo não está cheio
            if node.data[self.key][self.t*2-1] == 0:
                # Testando se é último elemento a ser adicionado
                i = node.getKey(data[self.key])[0]
                for index in range(self.struct.items()):
                    if index == self.child:
                        node.data[index].insert(i+1, data[index])
                        node.data[index].pop(self.t*2+1)
                    else:
                        node.data[index].insert(i, data[index])
                        node.data[index].pop(self.t*2)
                node.store()
                if node.father_ptr == 0:
                    self.root = node
                break
            else:
                # Testando se um nó é raiz
                if node.father_ptr == 0:
                    item, right = node.split(data)
                    data_node = [[x] for x in item]
                    root_ptr = node.newPtr() + self.block_s
                    right.father_ptr = root_ptr
                    node.father_ptr = root_ptr
                    right.store()
                    node.store()
                    self.root = _BNode(root_ptr, 0, False, self.init_node, data_node)
                    self.root.data[self.child].insert(0, node.ptr)
                    self.root.store()
                    self.root.becameRoot()
                    break
                else:
                    data, right = node.split(data)
                    node.store()
                    right.store()
                    node = self._blockCharge(node.father_ptr)
        return True

    def update(self, data):
        '''
            Utualiza o node com a chave primária que está dentro do dicionário data.
        '''
        data = self.struct.encodeItems(data)
        key = data[self.key]
        i, node = self._searchKey(key)
        if node.getKey(key)[1] == True:

            for i, item in node.data:
                item = data[i]
        else:
            return False

    def _blockCharge(self, ptr):
        '''
            Carrega um bloco do disco para a memória principal.
        '''
        with open(self.file, 'rb') as f:
            f.seek(ptr)
            data = f.read(self.block_s)
            father, last_child, leaf = unpack(NODE_STRUCT, data[:NODE_STRUCT_LEN])
            dummy = [[] for x in range(self.struct.items())]
            for piece in self._binChunck(data[NODE_STRUCT_LEN:]):
                items = unpack(self.struct.str, piece)
                for i, p in enumerate(items):
                    dummy[i].append(p)
            dummy[self.init_node['child']].append(last_child)

            node = _BNode(ptr, father, leaf, self.init_node, dummy)
        f.close()
        return node

    def _searchKey(self, key):
        '''
            Procura pela chave key na árvore
        '''
        i, equal = self.root.getKey(key)
        if self.root.leaf == True or equal == True:
            return (i, self.root)
        else:
            ptr = self.root.data[self.child][i]
            while True:
                node = self._blockCharge(ptr)
                i, equal = node.getKey(key)
                if node.leaf == True or equal == True:
                    return (i, node)
                else:
                    ptr = node.data[self.child][i]

    def _binChunck(self, data_bin):
        '''
            'Yielda' um dado binário em pacote de mesmo tamanho.
        '''
        for i in range(0, len(data_bin), len(self.struct)):
            yield data_bin[i:i+len(self.struct)]

class _BNode():
    '''
        Implementação de um nodo da árvore B.
    '''
    def __init__(self, ptr, father_ptr, leaf, kwargs, args=None):

        self.ptr = ptr
        self.father_ptr = father_ptr
        self.leaf = leaf

        self.t = kwargs['t']
        self.key = kwargs['key']
        self.child = kwargs['child']
        self.struct = kwargs['struct']
        self.file = kwargs['file']

        if args != None:
            self.data = args

            for i in range(len(self.data[self.key]), self.t*2):
                for item in self.data:
                    item.append(0)
            if len(self.data[self.child]) != self.t*2 + 1:
                self.data[self.child].append(0)

        else:
            self.data = [list() for i in range(self.struct.items())]

            for i in range(self.t*2):
                for item in self.data:
                    item.append(0)
            self.data[self.child].append(0)

    def __repr__(self):
        pstr = 'PTR: {} FAT_PTR:{} LEAF:{}'.format(self.ptr, self.father_ptr, self.leaf)
        pstr += '\nIDS: {}'.format(self.data[self.key]) + '\nCHILDREN: {}\n'.format(self.data[self.child])
        return pstr

    def getKey(self, key):
        '''
            Procura por um índice caso não encontre retorna a posição onde
            deve ser inserido o pesquisado pelo filho.
            Ao encontrar um índice 0 retorna esse índice já que está vazio.
        '''
        for i in range(self.t*2):
            a = self.data[self.key][i]
            if a == key:
                return (i, True)
            elif a == 0 or a > key:
                return (i, False)
        return (i+1, False)

    def saveNode(self, item, index):
        for i, field in enumerate(self.data):
            if i != self.child:
                field[index] = item[i]

    def getNode(self, i):
        return [x[i] for i,x in enumerate(self.data) if i == self.child]

    def getLastNode(self):
        for i, key in enumerate(self.data[self.key]):
            if key == 0:
                return [x[i-1] for i,x in enumerate(self.data) if i == self.child]

    def store(self):
        if self.ptr == 0:
            raise InvalidPointer('Tentando acessar um ponteiro físico que não existe')
        str_index = []
        for i, val in enumerate(self.struct.dic.values()):
            if val[:3] == 'str':
                str_index.append(i)
        with open(self.file, 'r+b')  as f:
            f.seek(self.ptr)
            f.write(pack(NODE_STRUCT, self.father_ptr, self.data[self.child][self.t*2], self.leaf))
            data_bin = b''
            for i in range(self.t*2):
                l = []
                for j, row in enumerate(self.data):
                    if j in str_index:
                        if row[i] == 0:
                            l.append(b'0')
                        else:
                            l.append(row[i])
                    else:
                        l.append(row[i])
                data_bin += pack(self.struct.str, *l)
            f.write(data_bin)
        f.close()

    def newPtr(self):
        with open(self.file, 'ab') as f:
            a = f.tell()
        f.close()
        return a

    def becameRoot(self):
        with open(self.file, 'r+b') as f:
            f.seek(0)
            f.write(pack('Q', self.ptr))
        f.close()

    def split(self, item):
        '''
            Quebra o Nodo em dois no índice index, além disso retira os campos
            de index dos dois nódos e retorna
            RETORNO:
                (dict['id', 'offset'] _BTreeNode(direita) _BTreeNode(esquerda))
        '''
        if len(item) != len(self.data):
            raise InvalidDataFormat('Dados estão em um formato incorreto.')

        info = {'t':self.t, 'key':self.key, 'child':self.child,
                'struct':self.struct, 'file':self.file}

        med_item = []
        new_data = [list() for i in self.data]
        new_ptr = self.newPtr()
        index = self.getKey(item[self.key])[0]
        for i, field in enumerate(item):
            if i == self.child:
                self.data[i].insert(index+1, field)
                med_item.append(new_ptr)
            else:
                self.data[i].insert(index, field)
                med_item.append(self.data[i].pop(self.t))

            for j in range(self.t, self.t*2):
                if i == self.child:
                    new_data[i].append(self.data[i][j+1])
                    self.data[i][j+1] = 0
                else:
                    new_data[i].append(self.data[i][j])
                    self.data[i][j] = 0
            if i == self.child:
                new_data[i].append(self.data[i][self.t*2+1])
                self.data[i][self.t*2+1] = 0
                self.data[i].pop(self.t*2+1)
        new_block = _BNode(new_ptr, self.father_ptr, self.leaf, info, new_data)

        return (med_item, new_block)

## ERROS
class InvalidKey(Exception):
    pass
class InvalidPrimaryKey(Exception):
    pass
class InvalidPointer(Exception):
    pass
class InvalidDataFormat(Exception):
    pass
