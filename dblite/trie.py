# -*- coding: utf-8 -*-
import sys, traceback
from .structtype import StructType
from struct import pack, unpack, calcsize
from collections import OrderedDict
from os import path, stat

class TrieTree(object):
    '''
        Implementação de uma árvore Trie em arquivo
        O primeiro nodo é sempre o root
        O root tem três dados a menos que os outros nodos
        A estrutura do root tem inteiros que identificam o índice do nodo que ele aponta dentro do texto
        Os demais nodos são os nodos de letras
        Assim como o root, os nodos apontam para outros nodos também
        Mas possuem três dados a mais:
            nodo[-1] -> É o índice atual do nodo no arquivo
            nodo[-2] -> É o índice atual do nodo que aponta para ele no arquivo
            nodo[-3] -> É o dado que deve ser retornado pelo nodo, caso ele seja uma palavra válida
        @param  string    Caminho para o arquivo da árvore
    '''
    def __init__(self, file_path):
        #Inicia as variáveis importantes para o funcionamento
        self.file_path = file_path                                  #Caminho para o arquivo onde iremos escrever a árvore trie
        self.first_letter = 'a'                                     #Letra usada para referência no cálculo dos apontamentos
        self.alphabet_size = 100                                    #Tamanho do nosso alfabeto, quantas letras iremos utilizar na árvore
        self.max_nodes = 'I'                                        #Número de nodos máximos que a árvore pode conter (unsigned int, 4 bytes de número de nodos, ou seja 2³² nodos ou caracteres)
        self.root_struct = str(self.alphabet_size)+self.max_nodes   #Estrutura da root para utilizar no pack() e unpack()
        self.root_size = calcsize(self.root_struct)                 #Tamanho em bytes da estrutura do root
        self.node_struct = self.root_struct+'III'                   #Estrutura dos nodos para utilizar no pack() e unpack()
        self.node_size = calcsize(self.node_struct)                 #Tamamho em bytes da estrutura dos nodos
        self.root = None                                            #Atributo que armazena informação do root
        self.last_index = 0                                         #Atributo que armazena o último index a ser inserido na árvore
        #Verificamos se a árvore já foi criada
        try:
            trie_file = open(self.file_path, 'rb')
        except IOError:
            #Se não foi, criamos ela e carregamos o root
            self.createTrie()
            self.loadRoot()
        else:
            #Se já foi, apenas fechamos o arquivo e carregamos o root
            trie_file.close()
            self.loadRoot()

    def __repr__(self):
        pt_str = 'TRIE at file: {}\nItens:\n'.format(self.file_path)
        pt_str += ', '.join([chr(i+ord(self.first_letter)) for i, x in enumerate(self.root) if x != 0])
        return pt_str

    '''
        Método para a criação do arquivo da árvore trie
    '''
    def createTrie(self):
        #Tentamos abrir o arquivo para escrita
        try:
            file = open(self.file_path, 'wb')
        except IOError:     #Se não abrir, informamos o erro
            print('Erro ao abrir arquivo!')
            Error.printLog(traceback.format_exc, 'Erro ao abrir arquivo.')  #Escreve no arquivo de log de erros
        else:
            #Se abrir, escrevemos um root vazio no início do arquivo
            file.write(pack(self.root_struct, *tuple(0 for x in range(0, self.alphabet_size))))
            file.close()
            self.searchLastIndex()  #E atualizamos o last_index

    '''
        Método para carregamento do root no atributo root
        Lê o root do início do arquivo
    '''
    def loadRoot(self):
        #tentamos abrir o arquivo para leitura
        try:
            file = open(self.file_path, 'rb')
        except IOError:     #Se não abrir, informamos o erro
            print('Erro ao abrir arquivo!')
            Error.printLog(traceback.format_exc, 'Erro ao abrir arquivo.')  #Escreve no arquivo de log de erros
        else:
            file.seek(0,0)                                  #Se abrir garante que está no início do arquivo
            output = file.read(self.root_size)              #Lê o root do arquivo
            self.root = unpack(self.root_struct, output)    #Passa as informações para o atributo root
            file.close()                                    #Fecha o arquivo

    '''
        Método para excluir palavra da árvore trie
        @param  string  Palavra a ser excluída
    '''
    def deleteWord(self, word):
        try:
            self.loadRoot()                         #Carregamos o root
            self.validateString(word)               #Validamos a string word
        except varTypeError as e:                   #Se não for string, lançamos uma exceção e printamos o erro no log de erros
            traceback.print_exc(file=sys.stdout)
            e.printLog()
        else:
            ptr = self.searchWord(word)                 #Pesquisamos a palavra na árvore
            if ptr != None:                             #Se acharmos ela
                end_word = 1                            #Verificamos se é um fim de palavra ou se existe alguma outra palavra que contenha ela
                for i in range(0, self.alphabet_size):
                    if ptr[i] != 0:
                        end_word = 0
                if end_word == 1:                       #Se for fim de palavra, excluimos os nodos que não serão utilizados
                    self.deleteNode(ptr, 0, end_word)
                else:
                    self.updateDataNode(ptr, 0)         #Se não for fim de palavra, apenas zera o dado do nodo

    '''
        Método para excluir nodos do arquivo da árvore
        @param  node_struct  Estrutura do nodo a ser excluído
        @param  int          Índice do nodo ao qual o nodo a ser excluído aponta
        @param  int          Se for 1 - Fim de palavra, se for 0 - Não é fim de palavra
    '''
    def deleteNode(self, ptr, next_index, end_word):
        try:
            file = open(self.file_path, 'r+b')  #Abrimos arquivo para leitura e escrita sem truncar ele
        except IOError:
            print('Erro ao abrir arquivo!')
            Error.printLog(traceback.format_exc, 'Erro ao abrir arquivo.')  #Escreve no arquivo de log de erros
        else:
            if ptr[-3] == 0 or end_word == 1:       #Apenas exclui o nodo se ele não possuir dado ou se for fim de palavra
                file.seek(self.root_size)           #Percorre o espaço do root no arquivo
                file.seek(ptr[-1]*self.node_size)   #Coloca o ponteiro de arquivo na posição do nodo que pretendemos apagar
                file.write(pack(self.node_struct, *tuple(0 for x in range(0, self.alphabet_size+3))))   #Truncamos o nodo no arquivo
                file.close()
                if ptr[-2] != 0:                        #Se o nodo que estamos excluindo for apontado por outro nodo
                    ptr = self.findNode(ptr[-2])        #Localizamos ele
                    self.deleteNode(ptr, ptr[-1], 0)    #E o excluímos
                else:                                           #Se o nodo que estamos excluindo for apontado pelo root
                    self.loadRoot()                             #Carregamos o root
                    for i in range(0, self.alphabet_size):      #Localizamos o apontador do nodo que queremos excluir
                        if self.root[i] == ptr[-1]:
                            self.updateNode(self.root, i, 0)    #E atualizamos o root, truncando o apontador do nodo que excluímos
            else:                                       #Se o nodo a ser excluído não for fim de palavra
                file.close()                            #Apenas truncamos o apontador pro nodo seguinte da palavra
                for i in range(0, self.alphabet_size):
                    if ptr[i] == next_index:
                        self.updateNode(ptr, i, 0)

    '''
        Método para atualizar dados de uma palavra da árvore trie
        @param  string          Palavra a ser atualizada
        @param  unsigned int    Dado a ser atualizado
    '''
    def updateWord(self, word, data):
        try:
            self.validateString(word)               #Valida a palavra para ver se é string
        except NotStringError as e:
            traceback.print_exc(file=sys.stdout)
            e.printLog()
        else:
            ptr = self.searchWord(word)             #Procuramos a palavra na árvore
            if ptr != None:
                self.updateDataNode(ptr, data)      #Se achar, atualizamos o último nodo da palavra

    '''
        Método para inserir palavra na árvore trie
        @param  string        Palavra a ser inserida
        @param  unsigne int   Dado a ser inserido
    '''
    def insertWord(self, word, data):
        try:
            self.validateString(word)               #Valida a string
        except NotStringError as e:
            traceback.print_exc(file=sys.stdout)
            e.printLog()
        else:
            ptr = self.searchWord(word)             #Procura a palavra na árvore
            if ptr == None:
                self.insertNode(word, data)         #Se ela não existir, insere os nodos
                return True
            else:
                return False

    '''
        Método para inserir nodos na árvore trie
        @param  string        Palavra a ser inserida
        @param  unsigne int   Dado a ser inserido
    '''
    def insertNode(self, word, data):
        ptr = self.root
        file = open(self.file_path, 'a+b')
        for i in word:
            if ptr[ord(i)-ord(self.first_letter)] == 0:
                if len(ptr) == self.alphabet_size:
                    previous_index = 0
                else:
                    previous_index = ptr[-1]
                self.searchLastIndex()
                self.updateNode(ptr, ord(i)-ord(self.first_letter), self.last_index)
                ptr = list(0 for x in range(0, self.alphabet_size+3))
                ptr[-1] = self.last_index
                ptr[-2] = previous_index
                file.write(pack(self.node_struct, *ptr))
                file.flush()
            else:
                ptr = self.findNode(ptr[ord(i)-ord(self.first_letter)])
        file.close()
        self.updateDataNode(ptr, data)


    def updateNode(self, ptr, local, index):
        file = open(self.file_path, 'r+b')
        file.seek(0, 0)
        if not (len(ptr) == self.alphabet_size):
            file.seek(self.root_size)
            file.seek(ptr[-1]*self.node_size)
            ptr = list(ptr)
            ptr[local] = index
            file.write(pack(self.node_struct, *ptr))
            file.flush()
        else:
            ptr = list(ptr)
            ptr[local] = index
            file.write(pack(self.root_struct, *ptr))
            file.flush()
            self.loadRoot()
        file.close()

    def updateDataNode(self, ptr, data):
        file = open(self.file_path, 'r+b')
        file.seek(self.root_size)
        file.seek(ptr[-1]*self.node_size)
        ptr = list(ptr)
        ptr[-3] = data
        file.write(pack(self.node_struct, *ptr))
        file.close()

    def listAll(self, **kwargs):
        self.loadRoot()
        listWords = []
        word = ''
        def recursiveKey(ptr, index, a, atual_letter):
            a += chr(atual_letter+ord(self.first_letter))
            ptr = self.findNode(index)
            if ptr[-3] != 0:
                listWords.append(a)
            for key, j in enumerate(ptr[:-3]):
                if j != 0:
                    recursiveKey(ptr, j, a, key)

        def recursiveOffset(ptr, index, a, atual_letter):
            a += chr(atual_letter+ord(self.first_letter))
            ptr = self.findNode(index)
            if ptr[-3] != 0:
                listWords.append(int(ptr[-3]))
            for key, j in enumerate(ptr[:-3]):
                if j != 0:
                    recursiveOffset(ptr, j, a, key)

        if kwargs.get('type', 'word') == 'word':
            for key, index in enumerate(self.root):
                word = ''
                if index != 0:
                    recursiveKey(self.root, index, word, key)
        elif kwargs.get('type', 'word') == 'offset':
            for key, index in enumerate(self.root):
                word = ''
                if index != 0:
                    recursiveOffset(self.root, index, word, key)

        return listWords

    def searchWord(self, word, returnPointer = False):
        ptr = self.root
        try:
            self.validateString(word)
        except IOError:
            print('Erro ao abrir arquivo!')
            Error.printLog(traceback.format_exc, 'Erro ao abrir arquivo.')
        #except NotStringError as e:
            #traceback.print_exc(file=sys.stdout)
            #e.printLog()
        else:
            for i in word:
                if ptr:
                    if ptr[ord(i)-ord(self.first_letter)] == 0:
                        return None
                    else:
                        ptr = self.findNode(ptr[ord(i)-ord(self.first_letter)])
                else:
                    return None
            if returnPointer:
                return ptr
            else:
                if ptr[-3] != 0:
                    return ptr[-3]
                else:
                    return None

    def searchPrefix(self, prefix, info = 'words'):
        listWords = []
        ptr = self.searchWord(prefix, True)
        if ptr:
            if ptr[-3] != 0:
                if info == 'words':
                    listWords.append(prefix)
                else:
                    listWords.append(ptr[-3])
        word = prefix
        def recursive(ptr, index, a, atual_letter):
            a += chr(atual_letter+ord(self.first_letter))
            ptr = self.findNode(index)
            if ptr[-3] != 0:
                if info == 'words':
                    listWords.append(a)
                elif info == 'offset':
                    listWords.append(ptr[-3])
            for key, j in enumerate(ptr[:-3]):
                if j != 0:
                    recursive(ptr, j, a, key)
        if ptr:
            for key, i in enumerate(ptr[:-3]):
                word = prefix
                if i != 0:
                    recursive(ptr, i, word, key)
        return listWords

    def findNode(self, index):
        file = open(self.file_path, 'rb')
        file.seek(self.root_size)
        file.seek((index)*self.node_size)
        output = file.read(self.node_size)
        ptr = unpack(self.node_struct, output)
        file.close()
        return ptr

    def searchLastIndex(self):
        file_size = path.getsize(self.file_path)
        file_size -= self.root_size
        self.last_index = int(file_size/self.node_size)+1



    def validateString(self, word):
        if not isinstance(word, str):
            raise varTypeError(str(type(word)), 'str')

'''
a1.insertWord('geronimo pau no cu', 69)
a1 = TrieTree('arthur')
a1.deleteWord('bolas')
#print(a.root)
print(a1.searchWord('geronimo pau no cu'))
'''
