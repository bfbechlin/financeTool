class Relation():
    '''
        Classe que concentra as operações entre as relações
    '''
    def __init__(self):
        self.relations = {'name':[], 'type':[]}

    def searchRelation(self, *args):
        '''
            Procura por uma relação e retorna seu índice.
            Existem duas formas de pesquisar:
                Nome de uma entidade: retorna o indice de todas as entidades
                    que possuem ela.
                Nome de duas entidades: retorna o indice da relação entre as
                    duas entidades, se não existir retorna None.
        '''
        if len(args) == 1:
            return [i for i, relation in enumerate(self.relations['name'])\
                    if str(args[0]) in relation]
        if len(args) == 2:
            for i, relation in enumerate(self.relations['name']):
                if args[0] in relation and args[1] in relation:
                    return i
            return None

    def createRelation(self, ent_1, ent_2):
        '''
            Cria uma relação entre duas entidades, é necessário enviar argumentos
            da seguinte forma
            (nomeEntidade1, tipoRelação1) ou [], (nomeEntidade2,tipoRelação2) ou []
        '''
        NAME = 0
        TYPE = 1
        ent_1_index = self.searchEntity(ent_1[NAME])
        ent_2_index = self.searchEntity(ent_2[NAME])
        if ent_1_index == None or ent_2_index == None:
            raise TypeError # erro ao criar relação, entidade/s que estão na relação não existe/m

        if self.searchRelation(ent_1[NAME], ent_2[NAME]) != None:
            raise TypeError # erro ao criar relação, as entidades já estão relacionadas

        def __appendRelation(ent_1, ent_2):
            self.relations['name'].append((ent_1[NAME], ent_2[NAME]))
            self.relations['type'].append((ent_1[TYPE], ent_2[TYPE]))
        def __appendRelationEntity(entity, index):
            self.entities['relations'][index].append(entity[NAME])
            self.entities['struct'][index] += 'Q'

        if str(ent_1[TYPE]) == 'N':
            if str(ent_2[TYPE]) == 'N':
                __appendRelation(ent_1, ent_2)
            elif str(ent_2[TYPE]) == '1':
                __appendRelation(ent_1, ent_2)
                __appendRelationEntity(ent_2, ent_1_index)
            else:
                raise TypeError # erro ao criar relação, tipo de relação inexistente
        elif str(ent_1[TYPE]) == '1':
            if str(ent_2[TYPE]) == 'N':
                __appendRelation(ent_2, ent_1)
                __appendRelationEntity(ent_1, ent_2_index)
            elif str(ent_2[TYPE]) == '1':
                __appendRelation(ent_2, ent_1)
                __appendRelationEntity(ent_2, ent_1_index)
                __appendRelationEntity(ent_1, ent_2_index)
            else:
                raise TypeError # erro ao criar relação, tipo de relação inexistente
        else:
            raise TypeError # erro ao criar relação, tipo de relação inexistente
