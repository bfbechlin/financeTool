import os

class Environment():
    '''
        Classe que concentra as operações com o ambiente do Banco de Dados
    '''
    def _setEnv(self):
        '''
            Configura toda árvore de arquivos e pasta que o banco de dados 
            necessita.
        '''
        for i, entity in enumerate(self.entities['name']):
            os.makedirs(self.db_dir + entity, exist_ok = True)
            f = open(self.mountPathSerial(entity), 'w')
            for field_name in self.entities['fields'][i].keys():
                f = open(self.mountPathField(entity, field_name), 'w')
                f.close()
        os.makedirs(self.db_dir + 'relations', exist_ok = True)
        for relation in self.relations['name']:
            f = open(self.mountPathRelation(relation), 'w')
            f.close()

    def mountPathSerial(self, entity):
        '''
            Monta o caminho para acesso do arquivo serial de uma entidade.
        '''
        sep = os.sep
        if self.searchEntity(entity) == None:
            raise TypeError # erro ao tentar acessar entidade
        return '{self.db_dir}{entity}{sep}{entity}.serial'.format(**locals())

    def mountPathField(self, entity, field):
        '''
           Monta o caminho para o acesso dos arquivos dos campos da entidade.
        '''
        if isinstance(entity, int):
            try:
                ent_index = entity
                entity = self.entities['name'][ent_index]
            except IndexError:
                raise TypeError # erro ao tentar acessar uma entidade, esse índice não existe
        else:
            ent_index = self.searchEntity(entity)
            if ent_index == None:
                raise TypeError # erro ao tentar acessar uma entidade, essa entidade não existe
        st_type = self.entities['fields'][ent_index].get(field, None)
        if st_type == None:
            raise TypeError # erro ao tentar acessar uma campo, esse campo não existe
        if st_type[:3] == 'str':
            ext = 'trie'
        else:
            ext = 'btree'
        sep = os.sep
        return '{self.db_dir}{entity}{sep}{field}.{ext}'.format(**locals())

    def mountPathRelation(self, relation):
        '''
           Método que devolve os caminhos para acessar arquivos internos do 
           banco de dados.
           Recebe o nome da entidade ou índice e o campo que deseja acessar.
        '''
        if isinstance(relation, int):
            try:
                rel_type = self.relations['type'][relation]
            except IndexError:
                raise TypeError # erro ao tentar acessar uma relação, esse índice não existe
        else:
            relation = self.searchRelation(*relation)
            if relation == None:
                raise TypeError # erro ao tentar acessar uma entidade, essa entidade não existe
            rel_type = self.relations['type'][relation]
        sep = os.sep
        if rel_type == ('N', 'N'):
            folder = 'relations'        
            file = '_'.join(self.relations['name'][relation])
        elif rel_type == ('N', '1'):
            folder = self.relations['name'][relation][0]
            file = self.relations['name'][relation][1]
        else: # 1 -->  1
            first = self.relations['name'][relation][0]
            second = self.relations['name'][relation][1]
            return ['{self.db_dir}{first}{sep}{second}.btree'.format(**locals()),
                    '{self.db_dir}{second}{sep}{first}.btree'.format(**locals())]
        return '{self.db_dir}{folder}{sep}{file}.btree'.format(**locals())

