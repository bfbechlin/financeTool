#-*- coding utf-8 -*-
from itertools import permutations
from struct import calcsize
from collections import OrderedDict

class StructType():
    '''
        Classe que gerencia os tipos de dados que podem ser criados para os demais
        módulos. Na realidade são quase todos os dados disponíveis pelo módulo
        struct que é usado para compactar os dados em dados binários.
    '''
    struct_types = {
            'char':('c', 0), 'schar':('b', 0), 'uchar':('B', 0), 'bool':('?', 1),
            'int2':('h', 2), 'uint2':('H', 2), 'int4':('i', 2), 'uint4':('I', 2),
            'long':('l', 2), 'ulong':('L', 2), 'int8':('q', 2), 'uint8':('Q', 2),
            'float':('f', 3), 'double':('d', 3), 'str':('s', 4)
            }

    def __init__(self, struct, minimize = False):
        '''
            Inicia uma estrutura de tipos. Necessário enviar um dicionário de
            estruturas da seguinte maneira:
                {'NOME DO CAMPO(1)':TIPO(1), 'NOME DO CAMPO(2)':TIPO(2)}
        '''
        if not isinstance(struct, dict):
            raise StructDictInvalid('Dicionário de estrutura inválido')

        self.struct_dict = OrderedDict(struct)
        self.struct_str = self.validateTypeDict()
        if minimize == True:
            self.minimize()

    def __repr__(self):
        '''
            Representa os dados como o dicionário de estrutura
        '''
        return '{}'.format(self.struct_dict)

    def __len__(self):
        '''
            Represente o tamanho da classe como o tamanho em bytes da estrutura
        '''
        return calcsize(self.struct_str)

    def __iter__(self):
        '''
            Itera sobre os itens do dicionário de estrutura
        '''
        for key in self.struct_dict.keys():
            yield key

    @property
    def dic(self):
        return self.struct_dict

    @property
    def str(self):
        return self.struct_str.encode('ASCII')

    def keys(self):
        return [x for x in self.struct_dict.keys()]

    def index(self, field):
        '''
            Retorna o índice de um campo, já que é um dicionário ordenado
        '''
        for i, key in enumerate(self.struct_dict.keys()):
            if key == field:
                return i
        raise KeyError(str(field))

    def items(self):
        '''
            Conta a quantidade de itens na estrutara de tipos
        '''
        return len(self.struct_dict.keys())

    def validateTypeDict(self):
        '''
            Recebe um dicionário de campos com tipos de dados e o valida.
            Retorna uma string que é usada para compactar os dados. Se houver
            um tipo de campo inválido levanta uma Exception
        '''
        struct_str = ''
        for struct_type in self.struct_dict.values():
            if struct_type in self.struct_types.keys():
                struct_str += self.struct_types[struct_type][0]
            elif struct_type[:3] == 'str':
                struct_str += struct_type[3:] + 's'
            else:
                raise UndefinedTypeStruct('Tipo de dados indefinido')
        return struct_str

    def encodeItems(self, items):
        '''
            Valida dados de entrada em relação ao tipo declarado para eles.
            Retornando uma lista ordenada com os dados
        '''
        if len(items) != self.items():
            raise DictDataError('Dicionário de dados não confere com o dicionário de estrutura.')
        buffer = []
        for field, st_type in zip(self.struct_dict.keys(), self.struct_dict.values()):
            try:
                if (st_type[:3]) == 'str':
                    instance = self.struct_types['str'][1]
                else:
                    instance = self.struct_types[st_type][1]
                item = items[field]
            except:
                raise DictDataError('Dicionário de dados não confere com o dicionário de estrutura.')
            try:
                if instance == 0:
                    item = str(item)
                    if len(items[field]) == 1:
                        buffer.append(item[field])
                    else:
                        raise DataTypeError('Tipo de dado não confere com o declarado.')
                elif instance == 1:
                    buffer.append(bool(item))
                elif instance == 2:
                    buffer.append(int(item))
                elif instance == 3:
                    buffer.append(float(item))
            except:
                raise DataTypeError('Tipo de dado não confere com o declarado.')
            if instance == 4:
                item = str(item)
                if int(st_type[3:]) >= len(item):
                    buffer.append(item.encode('UTF-8'))
                else:
                    raise DataTypeError('String maior do que a definida no dicionário de estrutura.')

        return buffer

    def decodeItems(self, items):
        '''
            Recebe uma lista ordenada e retorna um dicionário com os campos.
        '''
        buffer = {}
        if len(items) != self.items():
            raise DictDataError('Dicionário de dados não confere com o dicionário de estrutura.')
        try:
            for item, field in zip(items, self.struct_dict.keys()):
                key = self.struct_dict[field]
                if key[:3] == 'str':
                    key = 'str'
                instance = self.struct_types[key][1]
                if instance == 0:
                    buffer[field] = str(item)
                elif instance == 1:
                    buffer[field] = bool(item)
                elif instance == 2:
                    buffer[field] = int(item)
                elif instance == 3:
                    buffer[field] = float(item)
                elif instance == 4:
                    buffer[field] = item.decode('UTF-8')
        except:
            raise DataTypeError('Tipo de dado não confere com o declarado.')
        return buffer

    def minimize(self):
        '''
            Recebe um dicionário de campos com tipos de dados e o valida.
            Além disso, procura pela forma de estrutura que ocupa menor espaço
            em disco, retornando a string que codifica isso e a ordem das chaves
            que corresponde a isso.
        '''
        def permutationGen(set_elements):
            for x in permutations(set_elements, len(set_elements)):
                yield x

        def getStr(pairs):
            dummy = ''
            for x in pairs:
                dummy += str(x[1])
            return dummy

        # TIRANDO OS NÚMEROS DO TIPO
        # BUFFER (CAMPOS, STRING COD)
        buffer = []
        for st_field, st_type in\
                zip(self.struct_dict.keys(), self.struct_dict.values()):
            if st_type in self.struct_types.keys():
                buffer.append((st_field, self.struct_types[st_type][0]))
            elif st_type[:3] == 'str':
                buffer.append((st_field, st_type[3:] + 's'))
            else:
                raise UndefinedTypeStruct('Tipo de dados indefinido')

        pairs = buffer.copy()
        min_len = calcsize(getStr(buffer))
        for item in permutationGen(buffer):
            s = getStr(item)
            l = calcsize(s)
            if calcsize(s) < min_len:
                pairs = item
                min_len = l
        keys = [x[0] for x in pairs]
        st_dict = OrderedDict()
        for key in keys:
            st_dict[key] = self.struct_dict[key]
        self = self.__init__(st_dict)



    @classmethod
    def validateList(cls, struct_type, item):
        '''
            Valida um item verificando se ele possui mesmo o tipo de dado que ele
            declara ter.
        '''
        if isinstance(item, list) or isinstance(item, tuple):
            try:
                key = struct_type
                if key[:3] == 'str':
                    key = 'str'
                instance = cls.struct_types[key][1]
                if instance == 0:
                    buffer = [str(x) for x in item]
                    for item in buffer:
                        if len(item) != 1:
                            raise TypeError
                elif instance == 1:
                    buffer = [bool(x) for x in item]
                elif instance == 2:
                    buffer = [int(x) for x in item]
                elif instance == 3:
                    buffer = [float(x) for x in item]
                elif instance == 4:
                    buffer = [x.encode('UTF-8') for x in item]
                else:
                    raise TypeError
            except:
                raise DataTypeError('Tipo de dado não confere com o declarado.')
        else:
            raise DataTypeError('Não foi enviado uma lista/tupla válida.')
        return buffer

    @classmethod
    def validateItem(cls, struct_type, item):
        '''
            Valida um item verificando se ele possui mesmo o tipo de dado que ele
            declara ter.
        '''
        try:
            key = struct_type
            if key[:3] == 'str':
                key = 'str'
            instance = cls.struct_types[key][1]
            if instance == 0:
                buffer = str(item)
                if len(buffer) != 1:
                    raise TypeError
            elif instance == 1:
                buffer = bool(item)
            elif instance == 2:
                buffer = int(item)
            elif instance == 3:
                buffer = float(item)
            elif instance == 4:
                buffer = item.encode('UTF-8')
            else:
                raise TypeError
        except:
            raise DataTypeError('Tipo de dado não confere com o declarado.')
        return buffer

    @classmethod
    def validateType(cls, field):
        '''
            Recebe um tipo de dados e o valida.
            Retorna uma string que é usada para compactar os dados. Se não
            existir esse tipo levanta uma Exception
        '''
        if field in cls.struct_types.keys():
            struct_str = cls.struct_types[field][0]
        elif field[:3] != 'str':
            struct_str = field[3:] + 's'
        else:
            raise UndefinedTypeStruct('Tipo de dados indefinido.')
        return struct_str

## ERROS
class StructDictInvalid(Exception):
    pass
class UndefinedTypeStruct(Exception):
    pass
class DictDataError(Exception):
    pass
class DataTypeError(Exception):
    pass
