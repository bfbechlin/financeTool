#-*- coding utf-8 -*-
'''
    Codificação da data com um uint8 onde pode ser comparado
'''
dt_dict = {
    'year':     [12, 36],
    'month':    [4, 32],
    'day':      [5, 27],
    'hour':     [5, 22],
    'min':      [6, 16],
    'seg':      [6, 10],
    'millis':   [10, 0],
}

def dateTimePack(dict):
    cod = {
        'year':     [12, 36],
        'month':    [4, 32],
        'day':      [5, 27],
        'hour':     [5, 22],
        'min':      [6, 16],
        'seg':      [6, 10],
        'millis':   [10, 0],
        }

    for key in cod.keys():
        cod[key].append(int(dict.get(key, 0)))
    n = 0
    for data in cod.values():
        n += ((2**data[0] -1) & data[2]) << data[1]
    return n

def dateTimeUnpack(n):
    n = int(n)
    cod = {
        'year':     [12, 36],
        'month':    [4, 32],
        'day':      [5, 27],
        'hour':     [5, 22],
        'min':      [6, 16],
        'seg':      [6, 10],
        'millis':   [10, 0],
        }

    for key, val in zip(cod.keys(), cod.values()):
        cod[key] = (((2**val[0]-1) << val[1]) & n) >> val[1]
    return cod
