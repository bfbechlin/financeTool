from .structtype import StructType
from collections import OrderedDict

class Entity(StructType):
    '''
        Classe que concentra as operações entre as entidades
    '''
    def __init__(self):
        '''
            Construtor do gerenciador de entidades
            Cada entidade possui:
                * Nome: nome da entidade. (String)
                * Campos: campos que a entidade possui e seus tipos. (Dict)
                * Relações: relações Nx1 em que a entidade está. (List)
                * Estrutura: string que representada a estrutura binária. (Str)
        '''
        self.entities = {'name':[], 'fields':[], 'relations':[], 'struct':[]}
    
    def searchEntity(self, name):
        '''
            Procura por um entidade e retorna seu índice se existir.
        '''
        for i, entity in enumerate(self.entities['name']):
            if str(name) == entity:
                return i
        return None

    def createEntity(self, ent_name, fields):
        '''
            Cria um entidade com o nome ent_name, com os campos que devem estar
            dentro de um dicionário, sendo que para cada chave será o nome do 
            campo e seu valor a structType, ou seja, o tipo de dados que se usa.
            Para maiores informações sobre os tipos de dados veja na classe
            StructType.
        '''
        if self.searchEntity(ent_name) != None:
            raise TypeError # erro ao criar entidade, já existe uma entidade com o mesmo nome
        fields_lower = {str(x).lower():str(y).lower() 
                for x,y in zip(fields.keys(),fields.values())}
        field_dict = OrderedDict({'id':'unsigint4'})
        field_dict.update(fields_lower)
        self.entities['struct'].append(StructTypes(field_dict))
        self.entities['fields'].append(field_dict)
        self.entities['name'].append(str(ent_name))
        self.entities['relations'].append([])

