#-*- coding utf-8 -*-
from .structtype import StructType
from struct import calcsize, pack, unpack
from os import path

STRUCT_PTR = 'I'
NULL = 0
class HeapFile():
    '''
        Cria um arquivo serial. Argumentos
            * File: nome e caminho do arquivo (Default: new.list)
            * Struct: estrutura dos dados que serão armazenados (Default: {"id":"uint4"})
            Obs.: as listas sempre são gravadas em disco com block_size itens
            para evitar muita fragmentação. Determine seu tamanho de bloco pequeno
            caso suas listas sejam pequenas em média.
    '''
    def __init__(self, *args, **kwargs):
        if len(args) == 0:
            self.file = kwargs.get('file', 'new.heap')
            self.struct = kwargs.get('struct', '{"id":"uint4"}')
        else:
            self.file = args[0]
            self.struct = args[1]
        self.struct = StructType(self.struct, minimize = True)
        self.st_block = self.struct.struct_str
        self.st_block_size = calcsize(self.st_block)
        if not path.isfile(self.file):
            f = open(self.file, 'wb')
            f.close()

    def __repr__(self):
        return 'HEAPFILE at file: {}\nType:{}'.format(self.file, self.struct)

    def read(self, index):
        with open(self.file, 'rb') as f:
            f.seek((index-1)*self.st_block_size)
            output = f.read(self.st_block_size)
            ptr = unpack(self.st_block, output)
            ptr = self.struct.decodeItems(ptr)
        f.close()
        return ptr

    def insert(self, item):
        index = False
        with open(self.file, 'ab') as f:
            item = self.struct.encodeItems(item)
            f.write(pack(self.st_block, *item))
            index = int(f.tell()/self.st_block_size)
        f.close()
        return index

    def remove(self, index):
        with open(self.file, 'r+b') as f:
            f.seek((index-1)*self.st_block_size)
            ptr = self.read(index)
            for i in ptr:
                ptr[i] = 0
            ptr = self.struct.encodeItems(ptr)
            output = f.write(pack(self.st_block, *ptr))
        f.close()
