#-*- coding utf-8 -*-
from .structtype import StructType
from struct import calcsize, pack, unpack
from os import path

STRUCT_PTR = 'I'
NULL = 0
class DiskList():
    '''
        Cria um arquivo de listas em disco. Argumentos
            * File: nome e caminho do arquivo (Default: new.list)
            * Type: tipo dos dados que serão armazenados (Default: uint4)
            * Block_size: tamanho de bloco dos itens da lista. (Default: 10)
            Obs.: as listas sempre são gravadas em disco com block_size itens
            para evitar muita fragmentação. Determine seu tamanho de bloco pequeno
            caso suas listas sejam pequenas em média.
    '''
    def __init__(self, *args, **kwargs):
        if len(args) == 0:
            self.file = kwargs.get('file', 'new.list')
            self.type = kwargs.get('type', 'uint4')
            self.size = kwargs.get('block_size', 10)
        else:
            self.file = args[0]
            self.type = args[1]
            self.size = args[2]
        self.st_item = StructType.validateType(self.type)
        self.st_block = STRUCT_PTR + STRUCT_PTR + self.st_item*self.size
        if not path.isfile(self.file):
            f = open(self.file, 'wb')
            f.close()

    def __repr__(self):
        return 'DISKLIST at file: {}\nType:{}'.format(self.file, self.type)

    def create(self, item):
        '''
            Cria uma lista em disco. Pode receber um único item para inserir ou
            então uma lista de itens.
            Retorna um ponteiro para essa lista. Esse ponteiro é muito importante
            já que só com ele você consegue acessar a sua lista.
        '''
        ptr = self.newPtr()
        with open(self.file, 'r+b') as f:
            if isinstance(item, list) or isinstance(item, tuple):
                item = StructType.validateList(self.type, item)
                for data in self.makeBlock(ptr, item, new=False):
                    f.seek(data[1])
                    f.write(data[0])
            else:
                item = StructType.validateList(self.type, [item])
                data = self.makeBlock(ptr, item, new=False)[0]
                f.seek(data[1])
                f.write(data[0])
        return ptr

    def read(self, ptr, amount = 0, offsets = False):
        '''
            Lê a lista com o ponteiro ptr. Caso deseje ler uma quantia fixa de
            itens dessa lista use o parâmetro amount para especificar.
            Se quiseres todos os ponteiros dos blocos que a lista contem deixe
            o parâmetro offsets como True
        '''
        buffer = []
        local = []
        with open(self.file, 'rb') as f:
            f.seek(ptr)
            while True:
                data = unpack(self.st_block, f.read(calcsize(self.st_block)))
                local.append(data[1])
                for i in range(data[0]):
                    buffer.append(data[i+2])
                if data[0] != self.size:
                    break
                else:
                    if data[1] == NULL:
                        break
                    f.seek(data[1])
        f.close()
        if amount != 0:
            return buffer[:amount]
        else:
            if offsets == True:
                return buffer, local
            else:
                return buffer

    def append(self, ptr, item):
        '''
            Coloca elementos no fim da lista com ponteiro ptr. Pode ser um
            único item ou vários em forma de lista.
        '''
        last_ptr = self.findLastBlock(ptr)
        buffer = self.read(last_ptr)
        with open(self.file, 'r+b') as f:
            if isinstance(item, list) or isinstance(item, tuple):
                item = StructType.validateList(self.type, item)
            else:
                item = StructType.validateList(self.type, [item])
            buffer.extend(item)
            for data in self.makeBlock(last_ptr, buffer):
                f.seek(data[1])
                f.write(data[0])
        f.close()

    def remove(self, ptr, item):
        '''
            Remove elementos da lista com ponteiro ptr. Pode ser um único item
            ou vários em forma de lista.
        '''
        if isinstance(item, list) or isinstance(item, tuple):
            item = StructType.validateList(self.type, item)
        else:
            item = StructType.validateList(self.type, [item])
        items, ptrs = self.read(ptr, offsets = True)
        ptrs.insert(0, ptr)
        for a in item:
            items.remove(a)
        with open(self.file, 'r+b') as f:
            for data in self.makeDefBlock(ptrs, items):
                f.seek(data[1])
                f.write(data[0])
        f.close()

    def makeDefBlock(self, ptrs, items):
        '''
            Método interno para montar blocos do tamanho especificado e os
            compactar para binário. Recebe uma lista de itens e de ponteiros
            onde esses blocos estão.
        '''
        buffer = []
        for i, item in enumerate(self.iter(items)):
            size = len(item)
            for d in range(size, self.size):
                item.append(0)
            buffer.append([pack(self.st_block, size, ptrs[i+1], *item), ptrs[i]])
        buffer[-1][0] = pack(self.st_block, size, NULL, *item)
        return buffer

    def makeBlock(self, ptr, items, new=True):
        '''
            Método interno para montar blocos do tamanho especificado e os
            compactar para binário. Recebe uma lista de itens e o ponteiro
            inicial e caso no caso é necessário enviar o parâmetro new como
            Falso para que ele não crie dois blocos no mesmo lugar.
        '''
        if new:
            new_ptr = self.newPtr()
        else:
            new_ptr = ptr + calcsize(self.st_block)
        buffer = []
        ptrs = []
        for item in self.iter(items):
            size = len(item)
            for i in range(size, self.size):
                item.append(0)
            buffer.append([pack(self.st_block, size, new_ptr, *item), ptr])
            ptr = new_ptr
            new_ptr += calcsize(self.st_block)
        buffer[-1][0] = pack(self.st_block, size, NULL, *item)
        return buffer

    def findLastBlock(self, ptr):
        '''
            Encontra o último bloco de uma lista de ponteiro 'ptr'.
        '''
        last_ptr = ptr
        with open(self.file, 'rb') as f:
            f.seek(ptr)
            while True:
                data = unpack(self.st_block, f.read(calcsize(self.st_block)))
                new_ptr = data[1]
                if new_ptr != NULL:
                    f.seek(new_ptr)
                    last_ptr = new_ptr
                else:
                    break
        f.close()
        return last_ptr

    def newPtr(self):
        '''
            Cria um novo ponteiro. IMPORTANTE enquanto não salvar nessa posição
            todas vezes que for chamado esse método ele devolverá a mesma posição.
        '''
        with open(self.file, 'ab') as f:
            a = f.tell()
        f.close()
        return a

    def iter(self, items):
        '''
            Generator para iterar sobre listas para deixar de acordo com a estrutura
            interna
        '''
        for i in range(0, len(items), self.size):
            yield items[i:i+self.size]
