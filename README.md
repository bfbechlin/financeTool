O projeto desenvolvido usando python3 e foi dividido em duas partes, sendo elas:

* **DBLite:** implementação de estruturas de banco de dados estudadas em aula para
realizar o armazenamento dos dados da aplicação.

* **FinanceTool:** aplicação proposta para validar e testar essas estruturas
desenvolvidas

----

# DBLite

É um módulo python que contém a implementação para auxiliar armazenamento e recuperação de dados em disco. Todas as seguintes implementações são para estrutura de dados genéricos.

* **Btree:** uma estrutura em forma de árvore que usa propriedades de como os discos
mágneticos e seus controladores realizam o armazenamento de dados. Dentro dessa árvore
os dados são ordenados de acordo com um chave e podem conter um ou mais campos.
Nessa implementação poucos dados estão em memória de uso (RAM).

* **Trie:** implementação básica de uma árvore de prefixos para indexar dados do
tipo String.

* **HeapFile:** implementação de um arquivo serial, ou seja, um arquivo os registros são sempre salvos no fim. Muito útil já que seu armazenamento é muito rápido, todavia é necessário de estruturas externas para ajudar na recurperação de um registro.

* **DiskList:** implementação de um tabela de posting, ou seja, um lista de indices de registro que contém um determinado campo em comum.

* **DateTime:** cria um tipo de dados do tipo time, que compacta uma data e hora em um int 8 bytes podendo assim ser comparado com mais facilidade.

Existem módulos ainda em desenvolvimento são eles:
* **DB**: irá gerenciar todo o banco de dados
* **Entity:** irá gerenciar as entidades
* **Relation:** irá gerenciar as relações entre as entidades
* **Environment:** encarregado de criar e gerenciar os arquivos em disco.

Para uso dessas estruturas foi criado um módulo chamado *StructType* que gerencia todo os tipos de dados que podem ser usados nessa implementação de banco de dados.

## StructType

Classe que gerencia os tipos de dados que são suportados no *DBLite*.
Os dados são os seguintes e suas codificações:
* **Char:** 'char' (normal), 'schar'(com sinal), 'uchar'(sem sinal);
* **Boolean:** 'bool'
* **Int:** 'int2'(int 2 bytes), 'uint2'(int 2 bytes sem sinal), 'int4'(int 4 bytes), 'uint4'(int 4 bytes sem sinal),
'long'(int 4 bytes), 'ulong'(int 4 bytes sem sinal), 'int8':(int 8 bytes), 'uint8'(int 8 bytes sem sinal);
* **Float:** 'float':(float 4 bytes), 'double':(float 8 bytes)
* **String:** 'strX':(string com X byte)

O método *minimize* dessa classe é bastante importante para diminuir o espaço dessas estruturas em discos já que em python é necessário usar uma função
de um módulo disponível no seu *builtins* para poder compactar dados binários e para essa função a ordem dos argumentos faz diferença no tamanho que essa estrutura terá em disco.

## Btree
Para usar esse módulo é necessário iniciar sua classe da seguinte maneira:

``` python
Btree(file='CAMINHO DO ARQUIVO', fields={'CAMPO1':'TIPO DO CAMPO', ...}, primary = 'NOME DO CAMPO')
```
Todas as chaves tem um *default*:
* file: new.btree
* fields: {'id':'uint4'}
* primary: 'id'

Ex.:
``` python
from dblite import btree
test = BTree(fields={'id':'uint4', 'name':'str30'})
```
Métodos importantes da classe:

* btree.insert(data): necessário enviar um dicionário com os campos que deseja salvar, se a chave primária já estiver na árvore é retornado *False*.
* btree.update(data): procura pelo campo com chave a chave primárioa que está dentro dicionária *data* e atualiza os demais campos.
* btree.listAll(): lista todos os nodos da árvore em forma de uma lista de dicionparios.
* btree.search(key): procura por uma chave primária da árvore que seja igual a key. Caso não encontre retorna None
* btree.searchRange(key_ini, key_fin): lista todos os nós que têm a chave primária dentro do intervalo [key_ini:key_fin].

Ex.:
``` python
test.insert({'id':1, 'name':'testing'})
>> True
test.listAll()
>> [{'id':1, 'name':'testing'}]
test.insert({'id':2, 'name:':'testing2'})
>> True
test.insert({'id':20, 'name:':'testing20'})
>> True
test.search(20)
>> {'id': 20, 'name': 'testing20'}
test.searchRange(1,2)
>>[{'id': 1,
  'name': 'testing'},
 {'id': 2,
  'name': 'testing2'}]
```

# Fincance Tool

Para utilizar a interface criada no projeto é necessário ter instalar alguns pacotes de python, são eles:

* readchar
* matplotlib

Também é necessário estar um ambiente Unix-based. Para inicar o programa é necessário estar na raiz do projeto, pasta *Finance Tool* e então use o seguite comando:

``` Unix
python3 main.py --interface
```

## Formato CSV

O projeto suporta a importação de dados em arquivo format csv com os seguinte cabeçalho:

Date,Open,High,Low,Close,Volume,Adj Close
