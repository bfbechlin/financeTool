#-*- coding utf-8 -*-
from dblite import *
from collections import OrderedDict
from csv import DictReader

class StockQuotes():
    '''
        Monta ambiente com os pontos das ações
    '''
    def __init__(self, comp_serial):
        d = OrderedDict()
        d['id'] = 'uint4'
        # RELAÇÃO 1XN
        d['date'] = 'uint8'
        # RELAÇÃO 1XN
        d['company'] = 'uint4'

        d['open'] = 'float'
        d['high'] = 'float'
        d['low'] = 'float'
        d['close'] = 'float'
        d['volume'] = 'uint4'
        d['adj_close'] = 'float'
        self.serial = HeapFile(file = 'db/stockquotes.serial', struct = d)
        self.ids = 1

        e = OrderedDict()
        e['date'] = 'uint8'
        e['post'] = 'uint4'
        self.date = BTree(file = 'db/stockquotes_date.btree', fields = e, primary = 'date')
        self.datePost = DiskList(file = 'db/stockquotes_date.list', block_size = 10)

        e = OrderedDict()
        e['id'] = 'uint4'
        e['post'] = 'uint4'
        self.company = BTree(file = 'db/stockquotes_comp.btree', fields = e, primary = 'id')
        self.companyPost = DiskList(file = 'db/stockquotes_comp.list', block_size = 50)
        self.companySerial = comp_serial

    def insert(self, comp_index, file_path):
        '''
            Deve-se mandar validado
        '''
        file = open(file_path, newline='')
        stocks = DictReader(file)
        # TRAZENDO PARA MEMÓRIA IDS QUE COMPOEM A EMPRESA COM ESSE ID
        node = self.company.search(comp_index)
        comp_ids = []
        if node != None:
            comp_ids = self.companyPost.read(node['post'])
        non_inserts = 0
        inserts = 0
        for item in stocks:
            a = {'year': int(item['Date'][:4]), 'month': int(item['Date'][5:7]),
            'day': int(item['Date'][8:10]), 'hour': 0, 'min': 0, 'seg': 0, 'millis': 0}
            date = dateTimePack(a)
            d = {'id': self.ids, 'date': date, 'open': item['Open'], 'high': item['Open'],
                'low': item['Low'], 'close': item['Close'], 'volume': item['Volume'],
                'adj_close': item['Adj Close'], 'company': comp_index}
            INSERTED = False
            node = self.date.search(date)
            if node != None:
                post_list = self.datePost.read(node['post'])
                interset = list(set(comp_ids) & set(post_list))
                # DADO JÁ FOI INSERIDO
                if len(interset) != 0:
                    INSERTED = True
                    non_inserts += 1
                else:
                    index = self.serial.insert(d)
                    self.datePost.append(node['post'], index)
            else:
                index = self.serial.insert(d)
                post = self.datePost.create(index)
                self.date.insert({'date':date, 'post':post})
            if INSERTED == False:
                node = self.company.search(comp_index)
                if node != None:
                    self.companyPost.append(node['post'], index)
                else:
                    post = self.companyPost.create(index)
                    self.company.insert({'id':comp_index, 'post':post})
                self.ids += 1
                inserts += 1

        return (inserts, non_inserts)

    def list(self, **kwargs):
        '''
            Chaves: company, date
            Procura por empresa:
                lista de indices do arquivo serial das empresas
            Procura por data(CODIFICADAS):
                [data_inicial, data_final]
        '''
        stocks_date = []
        stocks_comp = []
        com_index = kwargs.get('company', False)
        date_tuple = kwargs.get('date', False)
        if com_index == False and date_tuple == False:
            raise SearchError('É necessário pesquisar por pelos um dos tipos: Nome da empresa ou Intervalo de Datas')
        if com_index != False:
            for index in com_index:
                stock_post_id = self.company.search(index)
                if stock_post_id != None:
                    stocks_comp.extend(self.companyPost.read(stock_post_id['post']))

        if date_tuple != False:
            date_in, date_fin = date_tuple
            dates = self.date.searchRange(date_in, date_fin)
            for date in dates:
                stocks_date.extend(self.datePost.read(date['post']))
        buffer = []
        if com_index != False and date_tuple != False:
            stock_list = list(set(stocks_date) & set(stocks_comp))
            for id in stock_list:
                buffer.append(self.serial.read(id))
        elif date_tuple != False:
            for id in stocks_date:
                buffer.append(self.serial.read(id))
        elif com_index != False:
            for id in stocks_comp:
                buffer.append(self.serial.read(id))
        return buffer

class SearchError(Exception):
    pass
