import matplotlib.pyplot as plt

def cruzou(curva1, curva2):
    """

    Retorna uma lista indicando em quais pontos houve cruzamento, ou não, entre duas listas fornecidas.

    Se não houver cruzamento entre as curvas entre os índices i e i+1, o campo de índice i+1 da lista de cruzamentos recebe o valor 0
    Se curva1 cruzar curva2 de baixo para cima entre os índices i e i+1, o campo de índice i+1 da lista de cruzamentos recebe o valor -1.    
    Se curva1 cruzar curva2 de cima para baixo entre os índices i e i+1, o campo de índice i+1 da lista de cruzamentos recebe o valor 1.
    Se as curvas possuirem um mesmo valor no índice i, então o índice i da lista de cruzamentos recebe o valor 2.

    As cruvas fornecidas devem compartilhar o mesmo eixo X.   
    
    """
    
    pos = curva1[0] - curva2[0]
    cruzamentos = []
    cruzamentos.append(0)
    
    if pos > 0:
        pos = 1
    elif pos < 0:
        pos = -1
    else:
        pos = 0
    
    for v1, v2 in zip(curva1[1:], curva2[1:]):
        new_pos = v1 - v2
        if new_pos > 0:
            new_pos = 1
        elif new_pos < 0:
            new_pos = -1
        else:
            new_pos = 0
            

        if new_pos == pos:
            cruzamentos.append(0)
        else:
            if (pos == 1 or pos == 0) and new_pos == -1:
                cruzamentos.append(-1)
            elif (pos == -1 or pos == 0) and new_pos == 1:
                cruzamentos.append(1)
            elif new_pos == 0:
                cruzamentos.append(2)

        pos = new_pos

    return cruzamentos


def mediaMovelExponencial(values, period):
    """

    Retorna uma lista com a média móvel exponencial para cada um dos valores fornecidos na lista values.
    Se o período for menor que a quantidade de valores, é impossivel calcular a MME e a função retorna [-1]
    Period é o periodo usado para o cálculo da média.
    
    """
    
    if period > len(values):
        return [-1]
    
    means = []
    means.append(values[0])

    total_values = len(values)
    k = 2/(period+1)
    for i in range(1, total_values):
        mean = values[i]*k + means[i-1]*(1-k)
        means.append(mean)

    return means


def IFR(values, period = 12):
    """

    Retorna uma lista com o IFR para os valores fornecidos. Para o cálculo do índice de uma data são necessários 'período' datas anteriores, portanto
    os valores começam informando o IFR da data inicial + período.
    Se o período for menor ou igual a quantidade de valores, é impossivel calcular o IFR e a função retorna [-1].
    Period é o periodo usado para o cálculo do indicador. O padrão é 12 dias.
    
    """
    
    total_values = len(values)
    if total_values <= period:
        return [-1]

    ifr_values = []
    
    pos_delta = 0
    neg_delta = 0
    for fech1, fech2 in zip(values, values[1:period+1]):
        delta = fech2 - fech1
        if delta >= 0:
            pos_delta += delta
        else:
            neg_delta -= delta

    pos_delta = pos_delta/period
    neg_delta = neg_delta/period
    
    if neg_delta == 0:
        ifr_values.append(100)
    else:
        ifr_values.append(100 - 100/(1 + pos_delta/neg_delta))

    for fech1, fech2 in zip(values[period:], values[period+1:]):
        delta = fech2 - fech1
        if delta >= 0:
            pos_delta = (pos_delta*(period-1) + delta)/period
        elif delta < 0:
            neg_delta = (neg_delta*(period-1) - delta)/period

        if neg_delta == 0:
            ifr_values.append(100)
        else:
            ifr_values.append(100 - 100/(1 + pos_delta/neg_delta))

    return ifr_values


def mediaMovelSimples(values, period = 5):
    """

    Retorna uma lista com a média móvel simples para os valores inseridos. Para calcular a MMS com período de p dias, são necessárias pelo menos p datas. Assim,
    o primeiro valor da lista retornada correspondem à data inicial + período - 1.
    Se o período for menor que o número de valores fornecidos, então é impossível calcular a MMS e a função retorna [-1]
    Period é o período usado para o cálculo da MMS. O padrão é 5 dias.
    
    """
    
    means = []
    summ = 0
    total_values = len(values)
    
    if period > total_values:
        return [-1]

    for val in values[:period]:
        summ += val

    means.append(summ/period)

    for new_val, last_val in zip(values[period:], values):
        summ = summ + new_val - last_val
        means.append(summ/period)

    return means


def estocasticoK(values, period=14):
    """

    Retorna uma lista com o Estocástico %K para os valores inseridos.
    Se o período for menor que o número de valores fornecidos, então a curva %K é calculada considerando o período igual ao número de valores fornecidos.    
    Period é o período usado para o cálculo do Estocástico %K. O padrão é 14 dias.

    """
    
    if len(values) < period:
        max_value = max(values)
        min_value = min(values)
        delta = max_value - min_value
        if delta == 0:
            return [0]*len(values)
        
        Kval = []
        for val in values:
            k = (val - min_value)/delta *100
            Kval.append(k)

        return Kval
    
    else:
        Kval = []
        len_values = len(values)
        
        max_values = max(values[:period])
        min_values = min(values[:period])
        delta = max_values - min_values
        if delta == 0:
            Kval += [0]*period
        else:
            for val in values[:period]:
                k = (val - min_values)/delta * 100
                Kval.append(k)

        old_id = 0
        for val in values[period:]:
            if values[old_id] == max_values or values[old_id] == min_values:
                max_values = max(values[old_id+1:old_id+period+1])
                min_values = min(values[old_id+1:old_id+period+1])
            elif val < min_values:
                min_values = val
            elif val > max_values:
                max_values = val
                
            old_id += 1

            delta = max_values - min_values
            if delta == 0:
                Kval.append(0)
            else:
                k = (val - min_values)/delta * 100
                Kval.append(k)

        return Kval


def estocasticoD(values, period=3):
    """

    Retorna uma lista com os valores do Estocástico %D para os valores fornecidos. Como esta curva é uma MMS com período P, se P for maior que o total de valores
    inseridos, a função retorna [-1].
    Period é o período usado para o cálculo do Estocástico %D. O padrão é 3 dias.

    """
    
    return mediaMovelSimples(values, 3)


def linhaMACD(values, period1 = 26, period2 = 12):
    """

    Retorna uma lista contendo a linha MACD para os valores inseridos. Como esta curva depende da MME, se os periodos inseridos forem maiores que o total de valores de values, então
    é impossivel calcular a linha MACD e a função retorna [-1].
    Period1 é o período usado na primeira MME e o padrão é 26 dias.
    Period2 é o período usado na segunda MME e o padrão é 12 dias.
    
    """
    
    exp_means_2 = mediaMovelExponencial(values, period2)
    exp_means_1 = mediaMovelExponencial(values, period1)
    
    if (exp_means_2 == [-1] or exp_means_1 == [-1]):
        return [-1]
    
    linha = []
    for vals2, vals1 in zip(exp_means_2, exp_means_1):
        linha.append(vals2 - vals1)

    return linha


def linhaDeSinalMACD(linha_MACD, period = 9):
    """

    Retorna uma lista contendo a linha de sinal MACD. O cálculo desta curva depende de uma linha MACD já cálculada que deve ser fornecida e, dentro da função, depende
    de uma MME. Assim, se a linha MACD inserida for inválida ou se o período inserido for maior que o total de valores da linha MACD, então a função retorna [-1]
    Period é o período para o cálculo da linha de sinal MACD. O período padrão é 9 dias.

    """
    
    if linha_MACD == [-1]: return [-1]
    
    return mediaMovelExponencial(linha_MACD, period)


def histogramaMACD(linha_macd, linha_de_sinal):
    """

    Retorna uma lista contendo os valores correspondentes ao histograma MACD. Como esses valores dependem de uma linha MACD e de uma linha de sinal MACD já calculadas,
    se uma delas for inválida a função retorna [-1]
    
    """
    if (linha_macd == [-1] or linha_de_sinal == [-1]):
        return [-1]

    
    histograma_macd = []
    for val1, val2 in zip(linha_macd, linha_de_sinal):
        histograma_macd.append(val1-val2)

    return histograma_macd


def analiseEstocastico(values, period_curva_D = 3):
    """

    Executa uma análise de compra e venda segundos os critérios do indicador Estocástico para os valores correspondentes ao preço de uma ação ao longo do tempo.
    Retorna uma lista com as ações ideais para os dias inseridos. Como o cálculo depende de uma MMS, o período inserido deve ser menor que o total de valores
    em values. Ainda pelo mesmo motivo, o primeiro sinal da lista retornada corresponde ao sinal entre a data inicial + period_curva_D -1 e a próxima data.
    period_curva_D é o período usado para o cálculo do Estocástico %D. O padrão é 3 dias.
    
    """

    if period_curva_D > len(values):
        return [-1]
    
    
    est_K = estocasticoK(values)
    est_D = estocasticoD(est_K, period_curva_D)
    cruzamentos = cruzou(est_K[period_curva_D-1:], est_D)
    
    analise = []
    for idx, inter in enumerate(cruzamentos):
        if inter == 1:
            analise.append("Sinal de Venda")
        elif inter == -1:
            analise.append("Sinal de Compra")
        else:
            analise.append("Sem sinal")

    return analise
