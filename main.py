from stockgraph import criaGraficos, salvaGraficos
from stockquotes import StockQuotes
from company import Company
from interface import *
from dblite import *
import os, sys

# INCIANDO CLASSES COM BANCO DE DADOS
os.makedirs('db', exist_ok=True)
os.makedirs('plot', exist_ok=True)
cp = Company()
st = StockQuotes(cp.serial)

def insertCompany():
    flag = True
    fields = 0
    while True:
        cp_name = insertCompanyName(not flag)
        if cp_name == False:
            return False
        else:
            cp_index = cp.name.searchWord(cp_name)
            if cp_index == None:
                break
            else:
                flag = False
    flag = False
    while True:
        date = insertCompanyFoundation(cp_name, flag)
        if date == False:
            return False
        else:
            cp_date = validateDate(date)
            if cp_date != False:
                break
            else:
                flag = True
    cp_date = dateTimePack(cp_date)
    cp.insert({'name':cp_name, 'foundation':cp_date})
    insertCompanySucess()
    return True

def listCompany():
    while True:
        opt = listCompanyMenu()
        if listCompanyItems(opt) != False:
            break

def listCompanyItems(opt):
    if opt == 0:
        cp_name = listCompanyName()
        if cp_name == False:
            return False
        elif cp_name[-1] == '*':
            wds = cp.name.searchPrefix(str(cp_name[:-1]), 'offset')
        else:
            wds = [cp.name.searchWord(cp_name)]
        reads = []
        for item in wds:
            reads.append(cp.serial.read(item))
        printPageCompany(reads, cp_name)
    elif opt == 1:
        flag = False
        while True:
            date_str = listCompanyFound(flag)
            if date_str == False:
                return False
            else:
                date = validateDate(date_str)
                if date == False:
                    flag = True
                else:
                    break
        post = cp.foundation.search(dateTimePack(date))
        if post == None:
            printPageCompany([], date_str)
        else:
            wds = cp.foundationPost.read(post['post'])
            reads = []
            for item in wds:
                reads.append(cp.serial.read(item))
            printPageCompany(reads, date_str)
    else:
        return

def insertStock():
    error = False
    comp_index = 0
    while True:
        comp_name = insertStockName(error)
        if comp_name == False:
            break
        else:
            comp_index = cp.name.searchWord(comp_name)
            if comp_index != None:
                break
            else:
                error = True
    if comp_index != 0 and comp_index != None:
        data = 0
        error = False
        while True:
            file = insertStockFile(comp_name, error)
            if file == False:
                break
            else:
                if not os.path.isfile(file):
                    error = True
                else:
                    data = st.insert(comp_index, file)
                    break
        if data == 0:
            insertStockError()
        else:
            insertStockSucess(data[0], data[1])

def listStock():
    MAX = 0xFFFFFFFFFFFFFFFF
    cp_name = listStockName()
    date_in = listStockDateIn(cp_name)
    if date_in == '':
        date_fin = listStockDateIn(cp_name, dateTimePack(0))
    else:
        date_fin = listStockDateIn(cp_name, dateTimePack(date_fin))

    if cp_name[-1] == '*':
        wds = cp.name.searchPrefix(str(cp_name[:-1]), 'offset')
    else:
        wds = [cp.name.searchWord(cp_name)]

    if date_in == '':
        date_in = 0
    if date_fin == '':
        date_fin = MAX

    interset = st.list(company = cp_name, date=[date_in, date_fin])
    reads = []
    for item in interset:
        reads.append(cp.serial.read(item))
    printPageStocks(reads, cp_name)

def makeGraph():
    error = False
    cp_index = 0
    while True:
        cp_name = graphGetName(error)
        if cp_name == False:
            break
        else:
            cp_index = cp.name.searchWord(cp_name)
            if cp_index != None:
                break
            else:
                error = True

    if cp_index != 0 and cp_index != None:
        flag = False
        date_in = 0
        date_fin = 0
        while True:
            while True:
                date_in = graphGetInDate(cp_name, flag)
                if date_in == False:
                    return False
                else:
                    date_in = validateDate(date_in)
                    if date_in != False:
                        break
                    else:
                        flag = True
            flag = False
            while True:
                date_fin = graphGetFinDate(cp_name, date_in, flag)
                if date_fin == False:
                    return False
                else:
                    date_fin = validateDate(date_fin)
                    if date_fin != False:
                        break
                    else:
                        flag = True
            data_in_pack = dateTimePack(date_in)
            data_fin_pack = dateTimePack(date_fin)
            nodes = st.list(company=[cp_index], date=[data_in_pack, data_fin_pack])
            if len(nodes) >= 30:
                break
            else:
                graphErrorData()
        selections = graphSelectPlots(cp_name, date_in, date_fin)
        if selections ==  False:
            return False
        else:
            file_name = '{}_DI{}_{}_{}DF_{}_{}_{}'.format(cp_name,
                date_in['day'], date_in['month'], date_in['year'],
                date_fin['day'], date_fin['month'], date_fin['year'])
            def packToPlot(stocks):
                values = [[], []]
                for i in stocks:
                    values[0].append(i['close'])
                    values[1].append(dateTimeUnpack(i['date']))
                return values
            graph = [i for i, x in enumerate(selections) if x == True]
            plots = criaGraficos(graph, packToPlot(nodes))
            names = salvaGraficos(plots)
            return True

def main():
    while True:
        opt = mainWindow()
        if opt == -1:
            break
        elif opt == 0:
            insertCompany()
        elif opt == 1:
            listCompany()
        elif opt == 2:
            insertStock()
        elif opt == 3:
            listStock()
        elif opt == 4:
            makeGraph()
        else:
            break
    os.system('clear')

if __name__ == '__main__':
    args = sys.argv
    if len(args) >= 2:
        if args[1] == '--interface':
            main()
    else:
        t = {
            'year':     2016,
            'month':    3,
            'day':      28,
            'hour':     0,
            'min':      0,
            'seg':      0,
            'millis':   0
            }
        a = dateTimePack(t)
        t['month'] = 5
        b = dateTimePack(t)
        def listStockCompany(cp_name):
            trie = cp.name.searchWord(cp_name)
            stocks = st.list(company = [trie])
            for i in stocks:
                for j in i:
                    if(j == 'date'):
                        i[j] = dateTimeUnpack(i[j])
                        i[j] = str(i[j]['day'])+'/'+str(i[j]['month'])+'/'+str(i[j]['year'])
                    print('{}: {} '.format(j, i[j]), end='')
                print(' ')

        def listStockDate(dates):
            stocks = st.list(date = dates)
            for i in stocks:
                for j in i:
                    if(j == 'date'):
                        i[j] = dateTimeUnpack(i[j])
                        i[j] = str(i[j]['day'])+'/'+str(i[j]['month'])+'/'+str(i[j]['year'])
                    print('{}: {} '.format(j, i[j]), end='')
                print(' ')

        def listStockDateCompany(cp_name, dates):
            trie = cp.name.searchWord(cp_name)
            stocks = st.list(date = dates, company = [trie])
            for i in stocks:
                for j in i:
                    if(j == 'date'):
                        i[j] = dateTimeUnpack(i[j])
                        i[j] = str(i[j]['day'])+'/'+str(i[j]['month'])+'/'+str(i[j]['year'])
                    print('{}: {} '.format(j, i[j]), end='')
                print(' ')
