from dblite import *
from stockquotes import StockQuotes
from company import Company
from terminalcolors import colorstring, colorprint
import readchar
import os, sys
from texttable import Texttable
def validateDate(date):
    '''
        DateFormat : dd/mm/aaaa
    '''
    if len(date) != 10:
        return False
    elif date[2:3] != '/' or date[5:6] != '/':
        return False
    a = {
        'year': date[6:10],
        'month': date[3:5],
        'day': date[0:2],
        'hour': 0,
        'min': 0,
        'seg': 0,
        'millis': 0
        }
    return a

def init():
    rows, columns = os.popen('stty size', 'r').read().split()
    print(columns)

def printHeader():
    colorprint('_'*80, attr='bold', fore='blue')
    colorprint('\t\t\t--: Finance Tool :--', attr='bold', fore='blue')
    colorprint('_'*80, attr='bold', fore='blue')
    print('\n')

def mainWindow():
    options = ['Inserir uma nova empresa', 'Listar empresas', 'Inserir ações',
    'Listar ações', 'Fazer gráficos']
    index = 0
    charac = 0
    while(charac != 'q' and charac != '\r'):
        os.system('clear')
        printHeader()
        print('\n')
        for i, ser in enumerate(options):
            print("\t\t{} {}".format(colorstring("-->", fore='blue') if index == i else "   " ,ser), flush=True)
        colorprint("\n\n\t\t\t\t\t\tPrescione 'q' caso deseje sair.", end ='', attr='bold')
        charac = readchar.readkey()
        if(charac == '\x1b[B'):
            if(index != len(options) - 1):
                index += 1
        if(charac == '\x1b[A'):
            if(index != 0):
                index -= 1
        if(charac == '\x03'):
            return -1

    if charac == 'q':
        return -1
    elif charac == '\r':
        return index

def insertCompanyName(error = False):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('')
    if error == True:
        colorprint('\tERRO: empresa já existe, insira novamente.', fore ='red')
    else:
        print('')
    comp = input('\tInsira o nome da empresa: ').lower()
    if comp == '':
        return False
    return comp

def insertCompanyFoundation(cp_name, error = False):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('\t{} {}'.format(colorstring('Empresa:', attr='bold'), cp_name))
    if error == True:
        colorprint('\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('')
    date = input('\tInsira a data de fundação(dd/mm/aaaa): ')
    if date == '':
        return False
    return date

def insertCompanySucess():
    os.system('clear')
    printHeader()
    print('\n')
    colorprint('\t\t\tEmpresa inserida com sucesso.\n', fore='green')
    input(colorstring('\n\n\t\t\t\t\t\t... Prescione qualquer tecla', attr = 'bold'))

def insertStockName(error):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('')
    if error == True:
        colorprint('\tERRO: empresa não existe, insira novamente.', fore ='red')
    else:
        print('')
    comp = input('\tInsira o nome da empresa: ').lower()
    if comp == '':
        return False
    return comp

def insertStockFile(cp_name, error):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('\t{} {}'.format(colorstring('Empresa:', attr='bold'), cp_name))
    if error == True:
        colorprint('\tERRO: arquivo não existe, insira novamente', fore ='red')
    else:
        print('')
    file = input('\tNome do arquivo (.csv): ')
    if file == '':
        return False
    return file

def insertStockSucess(inserteds, total):
    os.system('clear')
    printHeader()
    print('\n')
    colorprint('\t\t\tForam inseridos {} dados de um total de {}.'.format(inserteds, total),
        fore = ('green' if inserteds == total else 'red') )
    input(colorstring('\n\n\t\t\t\t\t\t... Prescione qualquer tecla', attr = 'bold'))

def insertStockError():
    os.system('clear')
    printHeader()
    print('\n')
    colorprint('\t\t\tHouve um erro inesperado, tente novamente.', fore = 'red')
    input(colorstring('\n\n\t\t\t\t\t\t... Prescione qualquer tecla', attr = 'bold'))

def graphGetName(error = False):
    os.system('clear')
    printHeader()
    print('\t\tPara voltar apenas tecle ENTER.')
    if error == True:
        colorprint('\n\tERRO: empresa não existe, insira novamente.', fore ='red')
    else:
        print('\n')
    comp = input('\tInsira o nome da empresa: ').lower()
    if comp == '':
        return False
    return comp

def graphGetInDate(cp_name, error = False):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('\t{} {}'.format(colorstring('Empresa:', attr='bold'), cp_name))
    if error == True:
        colorprint('\n\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('\n')
    date = input('\tInsira a data inicial(dd/mm/aaaa): ')
    if date == '':
        return False
    return date

def graphGetFinDate(cp_name, date_in, error=False):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('\t{empresa} {nome}\n\t{data_str}{day}/{month}/{year}'.format(
        empresa = colorstring('Empresa:', attr='bold'), nome = cp_name,
        day = date_in['day'], month = date_in['month'], year = date_in['year'],
        data_str = colorstring('Data Inicial:', attr='bold')))
    if error == True:
        colorprint('\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('')
    date = input('\tInsira a data final(dd/mm/aaaa): ')
    if date == '':
        return False
    return date

def graphSelectPlots(cp_name, date_in, date_fin):
    options = ['Preços', 'MMS', 'MME ', 'Estocástico %%K' , 'Estocástico %%K', 'Linha MACD', 'Linha de sinal MACD', 'Histograma MACD', 'IFR']
    selections = [True for i in range(len(options))]

    index = 0
    charac = 0

    while(charac != 'q' and charac != '\r'):
        os.system('clear')
        printHeader()
        print("\t\t\tPara voltar apenas tecle 'q'.")
        print('\t{empresa} {nome}\n\t{data_str1}{day_in}/{month_in}/{year_in}\t\t{data_str2}{day_fin}/{month_fin}/{year_fin}'.format(
            empresa = colorstring('Empresa:', attr='bold'), nome = cp_name,
            day_in = date_in['day'], month_in = date_in['month'], year_in = date_in['year'],
            day_fin = date_in['day'], month_fin = date_fin['month'], year_fin = date_in['year'],
            data_str1 = colorstring('Data Inicial:', attr='bold'), data_str2 = colorstring('Data Final:', attr='bold')))
        print('\n')
        for i, ser in enumerate(options):
            print("\t\t{} [{}] {}".format(colorstring("-->", fore='blue') if index == i else "   ",
                colorstring("X", fore='green') if selections[i] == True else " " ,ser), flush=True)
        colorprint("\n\n\t\t\t\t\tPara selecionar itens use ESPAÇO.", end ='', attr='bold')
        colorprint("\n\n\t\t\t\t\t\tPara finalizar use ENTER.", end ='', attr='bold')
        charac = readchar.readkey()
        if(charac == '\x1b[B'):
            if(index != len(options) - 1):
                index += 1
        if(charac == '\x1b[A'):
            if(index != 0):
                index -= 1
        if(charac == ' '):
            selections[index] = not selections[index]
    if charac == 'q':
        return False
    elif charac == '\r':
        return selections

def graphErrorData():
    os.system('clear')
    printHeader()
    print('\n')
    colorprint('\t\tDatas inseridas formam um intervalo com poucos dados.', fore = 'red')
    print('\t\tCaso tenha dúvidas liste os valores das ações.')
    input(colorstring('\n\n\t\t\t... Prescione qualquer tecla para redefinir as datas', attr = 'bold'))

def listCompanyMenu():
    options = ['Consulta por nome', 'Consulta por data de fundação']
    index = 0
    charac = 0
    while(charac != 'q' and charac != '\r'):
        os.system('clear')
        printHeader()
        print('\n')
        for i, ser in enumerate(options):
            print("\t\t{} {}".format(colorstring("-->", fore='blue') if index == i else "   " ,ser), flush=True)
        colorprint("\n\n\n\n\n\t\t\t\t\t\tPrescione 'q' caso deseje voltar.", end ='', attr='bold')
        charac = readchar.readkey()
        if(charac == '\x1b[B'):
            if(index != len(options) - 1):
                index += 1
        if(charac == '\x1b[A'):
            if(index != 0):
                index -= 1
        if(charac == '\x03'):
            return -1
    return index

def listCompanyName():
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('\tCaso deseje pesquisar por prefixo use * para identificar')
    print('')
    comp = input('\tInsira o nome da empresa: ').lower()
    if comp == '':
        return False
    return comp

def listCompanyFound(error):
    os.system('clear')
    printHeader()
    print('\t\t\tPara voltar apenas tecle ENTER.')
    print('')
    if error == True:
        colorprint('\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('')
    date = input('\tInsira a data de fundação(dd/mm/aaaa): ')
    if date == '':
        return False
    return date


def printPageCompany(values, search):
    lines = []
    for item in values:
        dict = dateTimeUnpack(item['foundation'])
        str = '{}/{}/{}'.format(dict['day'], dict['month'], dict['year'])
        lines.append([item['id'], item['name'], str])
    charac = 0
    while charac != '\r':
        os.system('clear')
        printHeader()
        print('\n\t\tPesquisa realizado por {}.'.format(search))
        colorprint('Resultados:', attr='bold')
        colorprint('\tID\t|\t  Name  \t|\tFoundation', attr ='bold')
        for i in lines:
            print('\t{}\t\t{}\t\t\t{}'.format(*i))
        if len(lines) == 0:
            colorprint("\t\t Consulta não obteve resultados.", fore ='red')
        colorprint("\n\n\t\t\t\t\tPrescione ENTER para finalizar consulta.", attr='bold')
        charac = readchar.readkey()

def listStockName():
    os.system('clear')
    printHeader()
    print('\tCaso não deseje filtrar por esse campo deixe ele vazio.')
    print('\tCaso deseje pesquisar por prefixo use * para identificar')
    comp = input('\tInsira o nome da empresa: ').lower()
    return comp

def listStockDateIn(cp_name):
    os.system('clear')
    printHeader()
    print('\tCaso não deseje filtrar por esse campo deixe ele vazio.')
    print('\t{} {}'.format(colorstring('Empresa:', attr='bold'), cp_name))
    if error == True:
        colorprint('\n\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('\n')
    date = input('\tInsira a data inicial(dd/mm/aaaa): ')
    if date == '':
        return False
    return date

def listStockDateFin(cp_name, date_in, error=False):

    os.system('clear')
    printHeader()
    print('\tCaso não deseje filtrar por esse campo deixe ele vazio.')
    print('\t{empresa} {nome}\n\t{data_str}{day}/{month}/{year}'.format(
        empresa = colorstring('Empresa:', attr='bold'), nome = cp_name,
        day = date_in['day'], month = date_in['month'], year = date_in['year'],
        data_str = colorstring('Data Inicial:', attr='bold')))
    if error == True:
        colorprint('\tERRO: data com formato inválido, insira novamente', fore ='red')
    else:
        print('')
    date = input('\tInsira a data final(dd/mm/aaaa): ')
    if date == '':
        return False

def printPageStocks(reads, cp_name, date_in, date_fin):
    while charac != '\r':
        os.system('clear')
        printHeader()
        print('\n\t{empresa} {nome}\n\t{data_str1}{day_in}/{month_in}/{year_in}\t\t{data_str2}{day_fin}/{month_fin}/{year_fin}'.format(
            empresa = colorstring('Empresa:', attr='bold'), nome = cp_name,
            day_in = date_in['day'], month_in = date_in['month'], year_in = date_in['year'],
            day_fin = date_in['day'], month_fin = date_fin['month'], year_fin = date_in['year'],
            data_str1 = colorstring('Data Inicial:', attr='bold'), data_str2 = colorstring('Data Final:', attr='bold')))
        colorprint('Resultados:', attr='bold')
        colorprint('\tID\t|\t  Preço  \t|\tFoundation', attr ='bold')
        for i in lines:
            print('\t{}\t\t{}\t\t\t{}'.format(*i))
        if len(lines) == 0:
            colorprint("\t\t Consulta não obteve resultados.", fore ='red')
        colorprint("\n\n\t\t\t\t\tPrescione ENTER para finalizar consulta.", attr='bold')
        charac = readchar.readkey()

    os.system('clear')
    printHeader()
