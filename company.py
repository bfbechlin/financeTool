from dblite import *
from collections import OrderedDict

class Company():

    def __init__(self):
        e = OrderedDict()
        e['id'] = 'uint4'
        e['name'] = 'str30'
        e['foundation'] = 'uint8'
        self.serial = HeapFile(file = 'db/company.serial', struct = e)
        self.ids = 1

        e = OrderedDict()
        e['foundation'] = 'uint8'
        e['post'] = 'uint4'
        self.foundation = BTree(file = 'db/company_found.btree', fields = e, primary = 'foundation')
        self.foundationPost = DiskList(file = 'db/company_found.list', block_size = 5)
        self.name = TrieTree('db/company_name.trie')

    def insert(self, dict):
        '''
            Recebe dicionário com chaves name(str30) e foundation(uint8)
        '''
        name = dict.get('name', False)
        date = dict.get('foundation', False)
        if name == False or date == False:
            self.ids -= 1
            raise InsertError("Os campos 'name' e 'foundation' são obrigatorios")
        name = name.lower()
        dict['id'] = self.ids
        self.ids += 1
        index = self.serial.insert(dict)
        if self.name.insertWord(name, index) == False:
            self.serial.remove(index)
            return False
        node = self.foundation.search(date)
        if node != None:
            self.foundationPost.append(node['post'], index)
        else:
            post = self.foundationPost.create(index)
            self.foundation.insert({'foundation':date, 'post':post})
        return True
        
    def remove(self, comp_name):
        '''
            Remove a empresa com o nome comp_name, se essa empresa não existir
            retorna False, caso tenha excluído retorna True
        '''
        i = self.name.searchWord(comp_name)
        if i == None:
            return False
        self.name.deleteWord(comp_name)
        comp = self.serial.read(i)
        self.serial.remove(i)
        date = self.foundation.search(comp['foundation'])
        self.foundationPost.remove(date['post'])
        return False

    def list(self, **kwargs):
        '''

        '''
        comp = []
        if kwargs.get('prefix', False):
            pass
        else:
            if kwargs.get('ordered', 'name') == 'name':
                list = self.name.listAll(type='offset')
                for offset in list:
                    comp.append(self.serial.read(offset))
                if kwargs.get('type', 'linear') == 'inverse':
                    comp.reverse()
            elif kwargs.get('ordered', 'name') == 'foundation':
                if kwargs.get('type', 'linear') == 'linear':
                    list = self.foundation.listAll()
                elif kwargs.get('type', 'linear') == 'inverse':
                    list = self.foundation.listAll(inverse = True)
                for data in list:
                    post = self.foundationPost.read(data['post'])
                    for ind in post:
                        a = self.serial.read(ind)
                        a['foundation'] = dateTimeUnpack(a['foundation'])
                        comp.append(a)
        return comp

    def listName(self, **kwargs):
        pass

## ERROS
class InsertError(Exception):
    pass
