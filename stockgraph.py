import stockanalysis as st
import matplotlib.dates as mdates
import datetime as dt
from matplotlib import pyplot as plt

def criaGraficos(tipos, val, nome_arquivo = "plot", period_IFR=12, period_MMS=5, period_MME=12, period_ESTK=14, period_ESTD=3):

    """

    Retorna uma lista contendo as figuras (gráficos contendo diferentes indicadores) criadas através da biblioteca matplotlib, o nome da figura e os indicadores
    reprentados na figura.
    Ex: plots = [ [ figMatPlotLib, nome_grafico, [indicador1, indicador2, ... ]], [ figMatPlotLib, nome_grafico, [indicador1, indicador2, ... ]], ...]

    Parametros
    ----------
    tipos : os indicadores que serão postos em gráfico
        preços = 0, MMS = 1, MME = 2, EST_K = 3, EST_D = 4, linha_MACD = 6, histograma_MACD = 7, IFRG = 8
    values : lista contendo duas listas, onde estão as datas e os preços de fechamento da ação para cada data.
        values[0] = preços de fechamento,  values[1] = datas correspondentes a cada preço
    nome_arquivo : é o prefixo para os nomes de gráficos que serão gerados. O nome de um gráfico é da forma [nome_arquivo]_[indicador1]_[indicador2]_...

    Na lista retornada, os indicadores contidos em cada gráfico são dados pelos seguintes nomes:
        preços = precos,  MMS = mms,  MME = mme,  EST_K = estk,  EST_D = estd,  linha MACD = macd, linha de sinal MACD = sinal_macd, histograma MACD = hist_macd,
        IFR = ifr

    """
    values = [[],[]]
    for i, colum in enumerate(val):
        for j, row in enumerate(colum):
            if i == 0:
                values[i].append(val[i][j])
            elif i == 1:
                str = '{}/{}/{}'.format(row['day'], row['month'], row['year'])
                values[i].append(dt.datetime.strptime(str,'%d/%m/%Y').date())

    plots = []
    graficos_criados = []
    nome_grafico = nome_arquivo
    graficos = []

    if 0 in tipos:
        fig = plt.figure(1)
        ax = fig.add_subplot(111)
        ax.plot(values[1], values[0], label='Fechamento')
        graficos_criados.append(0)
        nome_grafico += "_precos"
        graficos.append("precos")

    if 1 in tipos:
        mms = st.mediaMovelSimples(values[0], period=period_MMS)

        if mms != [-1]:
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            ax.plot(values[1][period_MMS-1:], mms, label='MMS')
            graficos_criados.append(1)
            nome_grafico += "_mms"
            graficos.append("mms")
        else:
            print("Impossível calcular a média móvel simples para os valores pedidos.")

    if 2 in tipos:
        mme = st.mediaMovelExponencial(values[0], period=period_MME)

        if mme != [-1]:
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            ax.plot(values[1], mme, label="MME")
            graficos_criados.append(2)
            nome_grafico += "_mme"
            graficos.append("mme")
        else:
            print("Impossível calcular a média móvel exponencial para os valores pedidos.")

    if (0 in graficos_criados) or (1 in graficos_criados) or (2 in graficos_criados):
        plt.legend(loc = 'upper left')
        plt.gcf().autofmt_xdate()
        plots.append([fig, nome_grafico, graficos])

    est_k = []
    nome_grafico = nome_arquivo
    graficos = []
    if 3 in tipos:
        est_k = st.estocasticoK(values[0], period=period_ESTK)

        if est_k != [-1]:
            fig = plt.figure(2)
            ax = fig.add_subplot(111)
            ax.plot(values[1], est_k, label="Est_K")

            graficos_criados.append(3)

            nome_grafico += "_estk"
            graficos.append("estk")
        else:
            print("Impossível calcular o estocástico K para os valores pedidos.")

    if 4 in tipos:
        if 3 not in graficos_criados:
            est_k = st.estocasticoK(values[0], period=period_ESTK)

        est_d = st.estocasticoD(est_k, period=period_ESTD)
        if est_d != [-1]:
            fig = plt.figure(2)
            ax = fig.add_subplot(111)
            ax.plot(values[1][2:], est_d, label= "Est_D")
            graficos_criados.append(4)
            nome_grafico += "_estd"
            graficos.append("estd")
        else:
            print("Impossível calcular o estocástico D para os valores pedidos.")

    if (3 in graficos_criados) or (4 in graficos_criados):
        plt.legend(loc = 'upper left')
        plt.gcf().autofmt_xdate()
        plots.append([fig, nome_grafico, graficos])

    linha_macd = []
    graficos = []
    nome_grafico = nome_arquivo
    if 5 in tipos:
        linha_macd = st.linhaMACD(values[0])
        if linha_macd != [-1]:
            fig = plt.figure(3)
            ax = fig.add_subplot(111)
            ax.plot(values[1], linha_macd, label="MACD")
            graficos_criados.append(5)

            nome_grafico += "_macd"
            graficos.append("macd")
        else:
            print("Impossivel calcular linha MACD para os valores pedidos.")

    linha_sinal_macd = []
    if 6 in tipos:
        if 5 not in graficos_criados:
            linha_macd = st.linhaMACD(values[0])

        linha_sinal_macd = st.linhaDeSinalMACD(linha_macd)
        if linha_sinal_macd != [-1]:
            fig = plt.figure(3)
            ax = fig.add_subplot(111)
            ax.plot(values[1], linha_sinal_macd, label="Sinal MACD")
            graficos_criados.append(6)

            nome_grafico += "_sinalmacd"
            graficos.append("sinal_macd")
        else:
            print("Impossível calcular a linha de sinal MACD para os valores pedidos.")

    if 7 in tipos:
        if not((5 in graficos_criados) or (6 in graficos_criados)):
            linha_macd = st.linhaMACD(values[0])

        if 6 not in graficos_criados:
            linha_sinal_macd = st.linhaDeSinalMACD(linha_macd)

        if not( linha_macd == [-1] or linha_sinal_macd == [-1]):
            fig = plt.figure(3)
            ax = fig.add_subplot(111)
            ax.plot(values[1], st.histogramaMACD(linha_macd, linha_sinal_macd), label="Hist MACD")
            graficos_criados.append(7)

            nome_grafico += "_histmacd"
            graficos.append("hist_macd")
        else:
            print("Impossível calcular o histograma MACD para os valores pedidos.")

    if (5 in graficos_criados) or (6 in graficos_criados) or (7 in graficos_criados):
        plt.legend(loc = 'upper left')
        plt.gcf().autofmt_xdate()
        plots.append([fig, nome_grafico, graficos])


    nome_grafico = nome_arquivo
    graficos = []
    if 8 in tipos:
        ifr = st.IFR(values[0], period_IFR)
        if ifr != [-1]:
            fig = plt.figure(4)
            ax = fig.add_subplot(111)
            ax.plot(values[1][period_IFR:],  ifr, label="IFR {}".format(period_IFR))
            graficos_criados.append(8)

            nome_grafico += "_ifr"
            graficos.append("ifr")
        else:
            print("Impossível calcular o IFR para os valores pedidos.")

    if 8 in graficos_criados:
        plt.legend(loc = 'upper left')
        plt.gcf().autofmt_xdate()
        plots.append([plt, nome_grafico, graficos])

    return plots


def salvaGraficos(plots):
    """

    Salva os gráficos na lista plot, que possui a mesma estrutura retornada pela função criaGraficos, com os nomes contidos na lista.

    """
    files = []
    for plot in plots:
        nome_arquivo = 'plots/' + plot[1] + ".png"
        plot[0].savefig(nome_arquivo)
        files.append(nome_arquivo)
    return
